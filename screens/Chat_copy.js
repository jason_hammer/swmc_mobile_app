//import React from 'react';
import React, { Component } from "react";
import {


  Text,
  View,
  StyleSheet,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
  TextInput,
  Keyboard
} from "react-native";
console.disableYellowBox = true;
import { StatusBar } from 'expo-status-bar';
import Footer1 from './Footer1';
import io from "socket.io-client";
import moment from 'moment';
import { LinearGradient } from 'expo-linear-gradient';

import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, FontAwesome, FontAwesome5,AntDesign } from '@expo/vector-icons';

import Constants from "expo-constants";
import { Linking } from 'react-native'
import TimeAgo from 'react-native-timeago';
// import { TextField } from "react-native-material-textfield";
import { Container, Input, Content, Button } from "native-base";
import { URL } from "../components/API";
import BaseHeader from "../components/BaseHeader";
import publicIP from 'react-native-public-ip';
import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
// import KeyboardSpacer from 'react-native-keyboard-spacer';
import Modal from 'react-native-modal';
import * as ImagePicker from 'expo-image-picker';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  MenuProvider
} from 'react-native-popup-menu';
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

export default class Chat_copy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      chat_id: 0,
      user_id: 0,
      user_name: "",
      user_image: "",
      messages: [],
      ip: "",
      department: "",
      departments_data: [],
      users: [],
      message: "",
      loan_number: "",
      phone: "",
      email: "",
      u_email: "",
      keyboardHeight: 0,
      showPopup: false,
      LoanModalVisible: false,
      PhoneModalVisible: false,
      EmailModalVisible: false,
      loan_num: "",
      message1: [],
    };

  }

  toggleLoaderModal = () => {
    this.setState({ isModalLoaderVisible: !this.state.isModalLoaderVisible });
  };

  componentDidMount() {
    AsyncStorage.getItem("other_data").then(department => {
      const dept = JSON.parse(department);
      //console.log("dept")
      //console.log(dept)
      this.setState({ department: dept.department, u_email: dept.email })
    })
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);

      if (val) {
        //console.log("val========================================================");
        //console.log(val);
        this.setState({
          user_name: val.name,
          chat_id: val.chat_id,
          user_id: val._id,

        });
        this.state.user_name = val.name;
        this.socket = io("https://serene-river-38311.herokuapp.com/");
        //console.log("------------------")
        //console.log(val.name)
        //console.log(this.state.department)
        //console.log("------------------")
        const email = this.state.u_email;
        const name = val.name;
        const room = this.state.department;


        // this.interval = setInterval(() => {
        //   //console.log("a")
        //   this.getMessages(val._id);
        // }, 1000);


        this.socket.emit('join', { name, room }, (error) => {
          if (error) {
            alert(error);
          } else {
            //console.log("mnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmn")
          }
        });
        this.socket.on("user_info_type", (res) => {

          //console.log("---------user_info_type-----------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log(res)
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("-------user_info_type-------------")
          // if (res.id === this.state.user_id.getItem('join_single_user_id')) {

          //   if (res.type === "loanNumber") {

          //     this.setState({ LoanModalVisible: !this.state.LoanModalVisible })
          //   }
          //   if (res.type === "phone") {

          //     this.setState({ PhoneModalVisible: !this.state.PhoneModalVisible })
          //   }
          //   if (res.type === "email") {

          //     this.setState({ EmailModalVisible: !this.state.EmailModalVisible })
          //   }

          // }
        });
        this.getMessages(val._id);
        this.socket.on("messages", msg => {
          this.getMessages(val._id);
          //console.log("---------messages-----------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log(msg)
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("--------------------")
          //console.log("-------messages-------------")
          this.setState({
            messages: [...this.state.messages, msg]
          });
        });

      }
    });
    AsyncStorage.getItem("user_image").then(image => {
      const val_image = JSON.parse(image);

      if (val_image) {
        //console.log("val========================================================");
        //console.log(val_image);
        //console.log("val========================================================");
        this.setState({
          user_image: val_image
        });
        this.state.user_avatar = val_image;

      }
    });
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
    this.getPermissionAsync();
    this.getCameraPermissionAsync();


  }
  getMessages = (id) => {
    fetch("https://serene-river-38311.herokuapp.com/api/chat/single/" + id, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        //console.log("-----------------FFFFFFFFFFFFFFFFFFFFFFFFF-------------------------")
        //console.log(response)
        //console.log("-----------------FFFFFFFFFFFFFFFFFFFFFFFFF----------------------")
        if (response.data) {
          this.setState({ messages: response.data })
        } else {
          Alert.alert("Sorry", "No Messages", [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }
  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }

    }

  };
  getCameraPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      if (status !== 'granted') {
        alert('Sorry, we need camera permissions to make this work!');
      }

    }

  }
  _pickImage = async () => {
    this.setState({ showPopup: false })
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        aspect: [4, 3],
        quality: 1,
        base64: true
      });
      if (!result.cancelled) {
        // this.setState({ image: result.uri });
        this.toggleLoaderModal();
        fetch(URL + "upload_file", {
          method: "POST",
          body: JSON.stringify({
            chat_id: this.state.chat_id,
            user_id: this.state.user_id,
            ip: this.state.ip,
            imageBase64: result.base64,

          }),
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(res => res.json())
          .then(async response => {
            //  //console.log("Send Image")
            //  //console.log(response)
            if (response.response == "success") {
              this.toggleLoaderModal();
            } else {
              this.toggleLoaderModal();
              Alert.alert("Sorry", response.message, [{ text: "OK" }], {
                cancelable: true
              });


            }
          })
          .catch(error => {
            this.toggleLoaderModal();
            //  //console.log("Please Check Your Internet Connection");

          });
      }

      //  //console.log(result);



    } catch (E) {
      //  //console.log(E);
    }
  };
  _scanImage = async () => {
    this.setState({ showPopup: false })
    try {
      let result = await ImagePicker.launchCameraAsync({
        allowsEditing: false,
        quality: 1,
        base64: true
      });
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        this.toggleLoaderModal();
        fetch(URL + "upload_file", {
          method: "POST",
          body: JSON.stringify({
            chat_id: this.state.chat_id,
            user_id: this.state.user_id,
            ip: this.state.ip,
            imageBase64: result.base64,

          }),
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(res => res.json())
          .then(async response => {
            //  //console.log("Send Image")
            //  //console.log(response)
            if (response.response == "success") {
              this.toggleLoaderModal();
              Alert.alert("Success", "File sent successfully.", [{ text: "OK" }], {
                cancelable: true
              });
            } else {
              this.toggleLoaderModal();
              Alert.alert("Sorry", response.message, [{ text: "OK" }], {
                cancelable: true
              });

            }
          })
          .catch(error => {
            this.toggleLoaderModal();
            //  //console.log("Please Check Your Internet Connection");

          });
      }

      //  //console.log(result);

    } catch (E) {
      alert(e);
    }
  };
  callDepartment = (phone) => {

    Linking.openURL(`tel:${phone}`)
  }

  getChat = () => {


  }

  submitmessage = () => {

    if (this.state.message != "") {

      //console.log(this.state.message);
      this.socket.emit('sendMessage', this.state.message, () => this.setState({ message: '' }));
    this.send_message_api()

    }
  
  }
  submit_loanNumber = (index) => {

    fetch("https://serene-river-38311.herokuapp.com/api/user/update/" + this.state.user_id, {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify({

        loanNumber: this.state.loan_number

      }

      )

    })


      .then(res => res.json())
      .then(async response => {
        //console.log("-----------------this.state.loan_number-------------------------")
        //console.log(this.state.loan_number)
        //console.log("-----------------this.state.loan_number----------------------")

        //console.log(">>>>>User Loan Number>>>>");
        //console.log(response);
        if (response) {
          this.setState({ LoanModalVisible: false })
        }
        let current_obj = this.state.messages[index]
        if (current_obj) {
          current_obj.loan_number = this.state.loan_number
          current_obj.enabled = true
        }
        // this.setState({ loan_number: "" })
        //console.log("this.state.messages[index]")
        //console.log(this.state.messages[index])
        // AsyncStorage.getItem("userID").then(user_data => {
        //   const val = JSON.parse(user_data);

        //   if (val) {
        //     AsyncStorage.setItem("userID", JSON.stringify(response.user_info));

        //   }
        // })

      })
    // .catch(error => {
    //   alert("Please check internet connection");
    // });

  }
  submit_phoneNumber = () => {
    fetch("https://serene-river-38311.herokuapp.com/api/user/update/" + this.state.user_id, {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        phone: this.state.phone

      })

    })
      .then(res => res.json())
      .then(async response => {
        //console.log(">>>>>User Phone Number>>>>");
        //console.log(response);
        if (response) {
          this.setState({ PhoneModalVisible: false })
        }
        // AsyncStorage.getItem("userID").then(user_data => {
        //   const val = JSON.parse(user_data);

        //   if (val) {
        //     AsyncStorage.setItem("userID", JSON.stringify(response.user_info));

        //   }
        // })

      })
    // .catch(error => {
    //   alert("Please check internet connection");
    // });

  }

  submit_emailAddress = () => {
    fetch("https://serene-river-38311.herokuapp.com/api/user/update/" + this.state.user_id, {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: this.state.email

      })

    })
      .then(res => res.json())
      .then(async response => {
        //console.log(">>>>>User Email Address>>>>");
        //console.log(response);
        if (response) {
          this.setState({ EmailModalVisible: false })
        }
        // AsyncStorage.getItem("userID").then(user_data => {
        //   const val = JSON.parse(user_data);

        //   if (val) {
        //     AsyncStorage.setItem("userID", JSON.stringify(response.user_info));

        //   }
        // })

      })
    // .catch(error => {
    //   alert("Please check internet connection");
    // });

  }

  send_message_api = () => {
    //console.log("https://serene-river-38311.herokuapp.com/api/user/show/"+this.state.user_id)

    fetch("https://serene-river-38311.herokuapp.com/api/user/show/" + this.state.user_id, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        //console.log("First")
        //console.log(response)
        if (response.data.joinedWith) {
          fetch(URL + "chat", {
            method: "POST",
            body: JSON.stringify({
              "from": this.state.user_id,
              "to": response.data.joinedWith,
              "body": this.state.message
            }),
            headers: {
              "Content-Type": "application/json"
            }
          })
            .then(res => res.json())
            .then(async response => {
              //console.log("Second")
              //console.log(response)
              if (response.data) {
                this.setState({ message: '' })
              } else {
                Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
                  cancelable: true
                });
                this.setState({ isLoading: false });
              }
            })
            .catch(error => {
              this.setState({ isLoading: false });
            });
        } else {
          Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });





  }
  exitChat = () => {
    Alert.alert(
      'Confirmation',
      'Are you sure you want to end this chat?',
      [
        {
          text: 'Yes', onPress: () => {
            AsyncStorage.clear();
            AsyncStorage.removeItem('userID');
            AsyncStorage.removeItem('user_image');
            this.props.navigation.push("Login");
          }
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        }

      ],
      { cancelable: false },
    );
  }

  _keyboardDidShow = e => {
    var KeyboardHeight = e.endCoordinates.height;
    this.setState({
      keyboardHeight: KeyboardHeight,
    });
  };
  _keyboardDidHide = e => {
    this.setState({
      keyboardHeight: 0,
    });
  };
  onFinish = (values) => {


    //console.log('values', values)
    if (values.loanNumber) {

      setOpenLoanNumberModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { loanNumber: values.loanNumber } }))
    }

    if (values.phoneNumber) {

      setOpenPhoneNumberModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { phone: values.phoneNumber } }))
    }

    if (values.email) {

      setOpenEmailModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { email: values.email } }))
    }


  };


  render() {
    const { navigation } = this.props.navigation;
    const fontColor = "#82a601";
    let image_path = this.state.user_image;
    let image_uri = "";
    image_uri = this.state.user_image

    //  if(image_uri){
    //   let ispresant=image_uri.indexOf("http");
    //   //console.log("-------1321313213--------")
    //   //console.log(ispresant)
    //   if(ispresant==-1){
    //     image_path="http://swmcapp.com/"+this.state.user_image;
    //   }else{
    //     image_path=image_uri
    //   }
    //  }
    // //console.log("final image_path")
    // //console.log(image_path)
    console.log("this.state.messages")
    console.log(this.state.messages)
    return (

      <MenuProvider style={{ width: deviceWidth, height: deviceHeight, backgroundColor: "#000" }}>


      
        <View style={{ width: "100%", flexDirection: "row", borderColor: "#DBDBDB", backgroundColor: "#000", paddingTop: 30, alignItems: "center", alignContent: "center" }}>
          <StatusBar style style="light" />
          {/* <TouchableOpacity style={{ width: "10%", marginTop: 10, paddingLeft: 10 }} onPress={() => this.props.navigation.push('profile')}>
            <Entypo name="home" size={20} color="#fff" />
          </TouchableOpacity> */}
          <View style={{ width: "80%", paddingBottom: 10, flexDirection: "row", marginRight: 15, marginTop: 6, paddingLeft: 10 }}>

            <View style={{ width: "12%", height: 20 }}>
              {/* <Image style={{width:40,height:40,borderRadius:100}} source={require('../assets/profile.png')} /> */}
              <Image style={styles.box} source={require('../assets/images/12.png')} />
            </View>
            {/*         
              <View style={{ width: "19%", alignItems: "center", height: 50, borderRadius: 50, borderWidth: 2, borderColor: "#fff" }}>
              </View> */}

            <View style={{ alignItems: "center", paddingTop: 10, }}>
              <Text style={{ fontSize: 16, color: "#fff", fontWeight: "bold" }}> {this.state.user_name}</Text>
            </View>
          </View>

          <TouchableOpacity style={{ width: "20%", alignItems: "center" }}>

            {/* <Menu >
              <MenuTrigger style={{ marginTop: 10 }} >
                <MaterialIcons name="more-horiz" size={20} color="#fff" />
              </MenuTrigger>
              <MenuOptions> */}
            {/* <MenuOption onSelect={() => this.exitChat()}  >
                  <Text style={{ color: '#003cff', fontSize: 17, fontWeight: "bold" }}>End Chat</Text>
                </MenuOption> */}
            {/* <MenuOption onSelect={() => alert(`Detail Updated`)} >
          <Text style={{color: '#003cff',fontSize:17,fontWeight:"bold"}}>Update Details</Text>
        </MenuOption> */}
            {/* </MenuOptions>
            </Menu> */}
          </TouchableOpacity>
        </View>
        <LinearGradient style={{ width: "100%", height: "90%", borderTopLeftRadius: 30, }} colors={['#f7bb97', '#dd5e89']}>
                        {/* //Loan */}
                          <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#25283a', borderRadius:10 }}>
                            <View style={{width:"20%",paddingLeft:5,paddingTop:5}}>
                            <Image style={styles.box} source={require('../assets/images/notifi.png')} />
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#6c6e77"}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#919299",fontSize:13,fontWeight:"bold"}}>
                               Enter your Loan Number
                                </Text>
                              </View>
                              <View style={{paddingTop:7,}}>
                              <View style={{width:"70%",backgroundColor:"#fff",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{padding:4}}>
                                <AntDesign name="checkcircle" size={15} color="#6c6c6c" />
                                </View>
                                  <View style={{ padding: 4 }}>
                                <TextInput
                                returnKeyType={"send"}
                                onSubmitEditing={() => this.submit_loanNumber(index)}
                                  keyboardType="default"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 13, paddingHorizontal: 10 }}
                                  placeholder={"Loan Number #"}
                                  placeholderTextColor="#bbbbbb"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={this.state.loan_number}
                                />
                                </View>
                                </View>
                              </View>
                              </View>
                              <View style={{width:"97%",paddingBottom:5}}>
                              <View style={{marginLeft:"auto"}}>
                              <AntDesign name="checkcircle" size={15} color="#16a917" />
                              </View>
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      




                        {/* //Cost */}
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#25283a', borderRadius:10,paddingBottom:15 }}>
                            <View style={{width:"20%",paddingLeft:5,paddingTop:5}}>
                            <Image style={styles.box} source={require('../assets/images/notifi.png')} />
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#6c6e77"}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#919299",fontSize:13,fontWeight:"bold"}}>
                             Cost Amount
                                </Text>
                              </View>
                              <View style={{paddingTop:7}}>
                              <View style={{width:"70%",backgroundColor:"#fff",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{padding:4}}>
                                <AntDesign name="checkcircle" size={15} color="#6c6c6c" />
                                </View>
                                  <View style={{ padding: 4 }}>
                                <TextInput
                                returnKeyType={"send"}
                                onSubmitEditing={() => this.submit_loanNumber(index)}
                                  keyboardType="default"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 13, paddingHorizontal: 10 }}
                                  placeholder={"Enter Cost"}
                                  placeholderTextColor="#bbbbbb"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={this.state.loan_number}
                                />
                                </View>
                                </View>
                              </View>
                              </View>
                      
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                       
                       {/* //EditeLoan Number */}
   <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#25283a', borderRadius:10,paddingBottom:15 }}>
                            <View style={{width:"20%",paddingLeft:5,paddingTop:5}}>
                            <Image style={styles.box} source={require('../assets/images/notifi.png')} />
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#6c6e77"}}>
                                  Edit Loan Number
                                </Text>
                              </View>
                           <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                              <View style={{width:"50%",backgroundColor:"#fff",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{padding:4}}>
                                <AntDesign name="checkcircle" size={15} color="#6c6c6c" />
                                </View>
                                  <View style={{ padding: 4 }}>
                                <TextInput
                                returnKeyType={"send"}
                                onSubmitEditing={() => this.submit_loanNumber(index)}
                                  keyboardType="default"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 13, paddingHorizontal: 10 }}
                                  placeholder={"44111"}
                                  placeholderTextColor="#bbbbbb"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={this.state.loan_number}
                                />
                                </View>
                                </View>
                              </View>
                              <View style={{marginHorizontal:6}}>
                              <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                              </View>
                              <View style={{width:"50%",backgroundColor:"#fff",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{padding:4}}>
                                <AntDesign name="checkcircle" size={15} color="#6c6c6c" />
                                </View>
                                  <View style={{ padding: 4 }}>
                                <TextInput
                                returnKeyType={"send"}
                                onSubmitEditing={() => this.submit_loanNumber(index)}
                                  keyboardType="default"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 13, paddingHorizontal: 10 }}
                                  placeholder={"44111"}
                                  placeholderTextColor="#bbbbbb"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={this.state.loan_number}
                                />
                                </View>
                                </View>
                              </View>

                           </View>
                            
                      
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
     {/* //Edit Cost Number */}
     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#25283a', borderRadius:10,paddingBottom:15 }}>
                            <View style={{width:"20%",paddingLeft:5,paddingTop:5}}>
                            <Image style={styles.box} source={require('../assets/images/notifi.png')} />
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#6c6e77"}}>
                                  Edit Cost Amount
                                </Text>
                              </View>
                           <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                              <View style={{width:"50%",backgroundColor:"#fff",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{padding:4}}>
                                <AntDesign name="checkcircle" size={15} color="#6c6c6c" />
                                </View>
                                  <View style={{ padding: 4 }}>
                                <TextInput
                                returnKeyType={"send"}
                                onSubmitEditing={() => this.submit_loanNumber(index)}
                                  keyboardType="default"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 13, paddingHorizontal: 10 }}
                                  placeholder={"44111"}
                                  placeholderTextColor="#bbbbbb"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={this.state.loan_number}
                                />
                                </View>
                                </View>
                              </View>
                              <View style={{marginHorizontal:6}}>
                              <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                              </View>
                              <View style={{width:"50%",backgroundColor:"#fff",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{padding:4}}>
                                <AntDesign name="checkcircle" size={15} color="#6c6c6c" />
                                </View>
                                  <View style={{ padding: 4 }}>
                                <TextInput
                                returnKeyType={"send"}
                                onSubmitEditing={() => this.submit_loanNumber(index)}
                                  keyboardType="default"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 13, paddingHorizontal: 10 }}
                                  placeholder={"44111"}
                                  placeholderTextColor="#bbbbbb"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={this.state.loan_number}
                                />
                                </View>
                                </View>
                              </View>

                           </View>
                            
                      
                             

                          

                            </View>

                        

                          </View>

                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
  














 {/* <Modal isVisible={this.state.LoanModalVisible}> */}
 <View style={{ flexDirection: "row-reverse", marginTop: 10, paddingHorizontal: 20 }}>
                            <View style={{ width: "auto", backgroundColor: "#fff", borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 3 }}>
                              <View style={{ padding: 10, flexDirection: "row" }}>
                                <TextInput
                                returnKeyType={"send"}
                                onSubmitEditing={() => this.submit_loanNumber(index)}
                                  keyboardType="default"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 15, paddingHorizontal: 10 }}
                                  placeholder={"Loan Number..."}
                                  placeholderTextColor="#bbbbbb"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={this.state.loan_number}
                                />
                                {/* <View style={{alignItems: "center", backgroundColor: "#FFf" }}> */}
                                {/* <TouchableOpacity style={{ padding: 5 }} onPress={() => this.submit_loanNumber(index)}>
                                  <FontAwesome name="send-o" size={15} color="#000" />
                                </TouchableOpacity> */}
                                {/* <TouchableOpacity style={{ padding:5 }} onPress={() => this.edit_message()}>
                      <FontAwesome5 name="edit" size={15} color="#000" />
                    </TouchableOpacity> */}

                                {/* </View> */}
                              </View>


                            </View>

                            <View style={{ width: "40%" }}>
                            </View>
                          </View>





                          <View key={index} >
                          {/* <Modal isVisible={this.state.LoanModalVisible}> */}
                          <View style={{ flexDirection: "row-reverse", marginTop: 10, paddingHorizontal: 20 }}>
                            <View style={{ width: "auto", backgroundColor: "#fff", opacity: 0.5, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 3 }}>
                              <View style={{ padding: 10, flexDirection: "row" }}>
                                <TextInput
                                  editable={false}
                                  keyboardType="number-pad"
                                  key={'input-def'}
                                  multiline={true}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#000', fontSize: 15, paddingHorizontal: 10 }}
                                  placeholder={"Loan Number..."}
                                  placeholderTextColor="#bbbbbb"

                                  value={item.toObj[0].loanNumber}
                                />
                                {/* <View style={{alignItems: "center", backgroundColor: "#FFf" }}> */}
                                <TouchableOpacity style={{ padding: 5 }}>
                                  <FontAwesome name="send-o" size={15} color="#000" />
                                </TouchableOpacity>
                                <TouchableOpacity style={{ padding: 5 }} >
                                  <FontAwesome5 name="edit" size={15} color="#000" />
                                </TouchableOpacity>

                                {/* </View> */}
                              </View>


                            </View>

                            <View style={{ width: "40%" }}>
                            </View>
                          </View>
                          </View>













                     



        </LinearGradient>


      </MenuProvider>
    )




  }

}
const styles = StyleSheet.create({

  chatbox: {

    width: "100%",
    marginLeft: 90,
    marginBottom: 50,

    paddingVertical: 15,

    backgroundColor: "#22c0f0",
    borderRadius: 50,
  },
  box: {

    width: "100%",
    
    resizeMode: "contain",
    borderRadius: 10
    // , borderWidth: 1, borderColor: "#f7bb97"



  },
  box1: {

    width: "95%",
    height: 30,
    resizeMode: "contain",

    // , borderWidth: 1, borderColor: "#f7bb97"



  },

})

// {this.state.user_image ? (
//   <View style={{ width: "19%", borderRadius: 50, borderWidth: 1, borderColor: "#fff", height: 50 }}>
//     {/* <Image style={{width:40,height:40,borderRadius:100}} source={require('../assets/profile.png')} /> */}
//     <Image style={styles.box} source={{ uri: this.state.user_image }} />
//   </View>
// ) : (
//   <View style={{ width: "19%", alignItems: "center", height: 50, borderRadius: 50, borderWidth: 2, borderColor: "#fff" }}>
//   </View>
// )}