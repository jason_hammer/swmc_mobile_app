//import React from 'react';
import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
  TextInput,
  Keyboard
} from "react-native";
console.disableYellowBox = true;
import { StatusBar } from 'expo-status-bar';
import Footer1 from './Footer1';
import io from "socket.io-client";
import moment from 'moment';
import { LinearGradient } from 'expo-linear-gradient';

import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, FontAwesome, FontAwesome5,AntDesign } from '@expo/vector-icons';

import Constants from "expo-constants";
import { Linking } from 'react-native'
import TimeAgo from 'react-native-timeago';
// import { TextField } from "react-native-material-textfield";
import { Container, Input, Content, Button } from "native-base";
import { URL, SWMC_ADMIN, SWMC_USER } from "../components/API";
import BaseHeader from "../components/BaseHeader";
import publicIP from 'react-native-public-ip';
import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
// import KeyboardSpacer from 'react-native-keyboard-spacer';
import Modal from 'react-native-modal';
import * as ImagePicker from 'expo-image-picker';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  MenuProvider
} from 'react-native-popup-menu';
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

export default class ChatRetro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      current_message_data: [],
      chat_id: 0,
      user_id: 0,
      avatar: "",
      full_name: "",
      user_email: "",
      user_name: "",
      toEdit: "",
      user_image: "",
      user_file: "",
      messages: [],
      ip: "",
      department: "",
      departments_data: [],
      users: [],
      message: "",
      loan_number: "",
      social_number: "",
      phone_number: "",
      email_number: "",
      dollar_number: "",
      percentage_number: "",
      password_number: "",
      phone: "",
      email: "",
      typing: false,
      percentage: "",
      u_email: "",
      keyboardHeight: 0,
      showPopup: false,
      LoanModalVisible: false,
      PhoneModalVisible: false,
      EmailModalVisible: false,
      isEditingCommand: false,
      loginModal: false,
      hideInput: false,
      isEditing: false,
      loan_num: "",
      email_err: "",
      commandID: "",
      commandType: "",
      name_err: "",
      message1: [],
    };

  }
  componentDidMount() {
  
    AsyncStorage.getItem("other_data").then(department => {
      const dept = JSON.parse(department);
      ////console.log("dept")
      ////console.log(dept)
      this.setState({ department: dept.department, u_email: dept.email })
    })
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);

      if (val) {
        console.log("val========================================================");
        console.log(val);
        console.log("val========================================================");
        this.setState({
          user_name: val.name,
          chat_id: val.chat_id,
          user_id: val._id,
          user_file: val.file,

        });
        this.state.user_name = val.name;
        this.socket = io("https://serene-river-38311.herokuapp.com/");
 
        ////console.log("------------------")
        ////console.log(val.name)
        ////console.log(this.state.department)
        ////console.log("------------------")
        const email = this.state.u_email;
        const name = val.name;
        const room = this.state.department;


        // this.interval = setInterval(() => {
        //   ////console.log("a")
        //   this.getMessages(val._id);
        // }, 1000);


        this.socket.emit('join', { name, room }, (error) => {
          if (error) {
            alert(error);
          } else {
            ////console.log("mnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmn")
          }
        });
        // io.on("connect", function (socket) {
          this.socket.on("typing_client_message",(res) => {
            this.socket.broadcast.emit("typing_client", res);
          console.log("typing_client_message Socket emited");
          });
          
          
          this.socket.on("typing_server_message",(res) => {
            this.socket.broadcast.emit("typing_server",res);
          console.log("typing_server_message Socket emited");
          });
        // });
        


  this.socket.on("typing_server", res => {
  
    if (this.state.user_id == res.to || this.state.user_id == res.from) {
      if (res.is_end == 1) {
        this.setState({typing:false});
      }
      else {
        this.setState({typing:true});
      }
      }
  
  console.log("Typing...");
  // console.log(res);
  });
  

        this.socket.on("user_info_type", (res) => {

          ////console.log("---------user_info_type-----------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log(res)
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("-------user_info_type-------------")
          // if (res.id === this.state.user_id.getItem('join_single_user_id')) {

          //   if (res.type === "loanNumber") {

          //     this.setState({ LoanModalVisible: !this.state.LoanModalVisible })
          //   }
          //   if (res.type === "phone") {

          //     this.setState({ PhoneModalVisible: !this.state.PhoneModalVisible })
          //   }
          //   if (res.type === "email") {

          //     this.setState({ EmailModalVisible: !this.state.EmailModalVisible })
          //   }

          // }
        });
        this.socket.on("typing_client", (res) => {

          if (res.is_end == 1) {
          this.setState({typing:false});
          }
          else {
            this.setState({typing:true});
        
          }
          });
         
        this.avatar();
        this.getMessages(val._id);
        this.socket.on("messages", msg => {
          this.getMessages(val._id);
          ////console.log("---------messages-----------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log(msg)
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("--------------------")
          ////console.log("-------messages-------------")
          this.setState({
            messages: [...this.state.messages, msg]
          });
        });

      }else{
        this.setState({loginModal:true})
      }
    });
    AsyncStorage.getItem("user_image").then(image => {
      const val_image = JSON.parse(image);

      if (val_image) {
        ////console.log("val========================================================");
        ////console.log(val_image);
        ////console.log("val========================================================");
        this.setState({
          user_image: val_image
        });
        // this.state.user_avatar = val_image;

      }
    });
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
    // this.default_Image();
    // this.getCameraPermissionAsync();


  }
  submit = () => {

    const { full_name, user_email,ip } = this.state;
let name = full_name;
let email = "user"+Math.floor(Math.random() * 
9999999)+"@swmc.com";
    let email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let l_email = email.toLowerCase();

    if (name == "" && email == "") {
      Alert.alert(
        "SWMC_app",
        "please enter all fields",
        [

          { text: "OK" }
        ],
        { cancelable: false }
      );
    }


    if (name == "") {
      this.setState({ name_err: "Required" });
    } else {
      this.setState({
        name_err: ""
      });
    }

    // if (email == "") {
    //   this.setState({ email_err: "Required" });
    // } else {
    //   if (l_email.indexOf("@swmc.com") == -1 ) {
    //     this.setState({
    //       email_err: "Invalid Email Address"
    //     });
    //   } else {
    //     this.setState({
    //       email_err: ""
    //     });
    //   }

    // }
    // if (department == "") {
    //   this.setState({ department_err: "Required" });
    // } else {
    //   this.setState({
    //     department_err: ""
    //   });
    // }

    if (name != "" && email != "") {
      this.setState({ isLoading: true });
      ////console.log(l_email+"--"+name+"--"+ip+"--"+department+"--"+this.state.images[this.state.pointer]);
      fetch(SWMC_USER + "user", {
        method: "POST",
        body: JSON.stringify({
          "email": l_email,
          "name": name,
          "image" : this.state.user_avatar,
          "ip" : ip,
          "department": "General",
          "phoneNumber" : "877878787",
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(async response => {
          ////console.log("Response =>=?");
          ////console.log(response);
          if (response.status != false) {
            ////console.log("this.state.address")
            ////console.log(this.state.address)
            ////console.log(this.state.department)
            // ////console.log("this.state.address")
            let arr = {};
            arr.address = this.state.address;
            arr.email = this.state.email;
            // arr.department = department;
            // var check_selected_array_object = this.state.departments_data.find(
            //   task => task._id === this.state.department
            // );
            // if(check_selected_array_object){

            //   arr.department= check_selected_array_object.name;
            // }else{
            //   arr.department= 'N/A';

            // }
            AsyncStorage.setItem("userID", JSON.stringify(response.data));
            AsyncStorage.setItem("user_image", JSON.stringify(this.state.user_avatar));
            AsyncStorage.setItem("other_data", JSON.stringify(arr));
            // ////console.log("arr")
            // ////console.log(arr)
            // AsyncStorage.setItem("user_image", JSON.stringify(this.state.images[this.state.pointer]));
            AsyncStorage.getItem("userID").then(user_data => {
              const val = JSON.parse(user_data);
        
              if (val) {
                ////console.log("val========================================================");
                ////console.log(val);
                this.setState({
                  user_name: val.name,
                  chat_id: val.chat_id,
                  user_id: val._id,
        
                });
                this.state.user_name = val.name;
                this.socket = io("https://serene-river-38311.herokuapp.com/");
                ////console.log("------------------")
                ////console.log(val.name)
                ////console.log(this.state.department)
                ////console.log("------------------")
                const email = this.state.u_email;
                const name = val.name;
                const room = this.state.department;
        
        
                // this.interval = setInterval(() => {
                //   ////console.log("a")
                //   this.getMessages(val._id);
                // }, 1000);
        
        
                this.socket.emit('join', { name, room }, (error) => {
                  if (error) {
                    alert(error);
                  } else {
                    ////console.log("mnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmnmn")
                  }
                });
                this.socket.on("user_info_type", (res) => {
        
                  ////console.log("---------user_info_type-----------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log(res)
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("-------user_info_type-------------")
                  // if (res.id === this.state.user_id.getItem('join_single_user_id')) {
        
                  //   if (res.type === "loanNumber") {
        
                  //     this.setState({ LoanModalVisible: !this.state.LoanModalVisible })
                  //   }
                  //   if (res.type === "phone") {
        
                  //     this.setState({ PhoneModalVisible: !this.state.PhoneModalVisible })
                  //   }
                  //   if (res.type === "email") {
        
                  //     this.setState({ EmailModalVisible: !this.state.EmailModalVisible })
                  //   }
        
                  // }
                });
                this.avatar();
                this.getMessages(val._id);
                this.socket.on("messages", msg => {
                  this.getMessages(val._id);
                  ////console.log("---------messages-----------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log(msg)
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("--------------------")
                  ////console.log("-------messages-------------")
                  this.setState({
                    messages: [...this.state.messages, msg]
                  });
                });
        
              }else{
                this.setState({loginModal:true})
              }
            });
            AsyncStorage.getItem("other_data").then(department => {
              const dept = JSON.parse(department);
              ////console.log("dept")
              ////console.log(dept)
              if(dept){

                this.setState({ department: dept.department, u_email: dept.email })
              }
            })
            AsyncStorage.getItem("user_image").then(image => {
              const val_image = JSON.parse(image);
        
              if (val_image) {
                ////console.log("val========================================================");
                ////console.log(val_image);
                ////console.log("val========================================================");
                this.setState({
                  user_image: val_image
                });
                // this.state.user_avatar = val_image;
        
              }
            });
            this.setState({ isLoading: false, loginModal:false });
            this.props.navigation.push("ChatRetro")
          } else {
            Alert.alert("Sorry", response.msg, [{ text: "OK" }], {
              cancelable: true
            });
            this.setState({ isLoading: false });
          }
        })
        .catch(error => {
          // ////console.log("Please Check Your Internet Connection");
          this.setState({ isLoading: false });
        });
    }
  }
  toggleLoaderModal = () => {
    this.setState({ isModalLoaderVisible: !this.state.isModalLoaderVisible });
  };
  
  getMessages = (id) => {
    fetch("https://serene-river-38311.herokuapp.com/api/chat/single/" + id, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
       //console.log("-----------------FFFFFFFFFFFFFFFFFFFFFFFFF-------------------------")
       //console.log(response)
       //console.log("-----------------FFFFFFFFFFFFFFFFFFFFFFFFF----------------------")
        if (response.data) {
          this.setState({ messages: response.data })
        } else {
          Alert.alert("Sorry", "No Messages", [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }
  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }

    }

  };
  getCameraPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      if (status !== 'granted') {
        alert('Sorry, we need camera permissions to make this work!');
      }

    }

  }
  
  postAvatarImage = (data) => {
    const photo = {
      uri: data.uri,
      type: "image/jpeg",
      name: "photo"+this.state.user_id+".jpeg",
    };

    const form = new FormData();

    form.append("file", photo);
  console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa")
  console.log(this.state.user_id)
  console.log(form)
  console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa")
    fetch("https://serene-river-38311.herokuapp.com/api/user/update/"+this.state.user_id , {
      method: "POST",
      body : form,
    })
      .then(res => res.json())
      .then(async response => {
       console.log(">>>>>>>>>>>>>>>>>>RESPONSE>>>>>>>>>>>>>>>>");
       console.log(response);
       console.log(">>>>>>>>>>>>>>>>>>RESPONSE>>>>>>>>>>>>>>>>");
       AsyncStorage.getItem("userID").then(user_data => {
        const val = JSON.parse(user_data);
  
        if (val) {
          val.file = response.data.user_info.file

          AsyncStorage.setItem("userID", JSON.stringify(val));
        }
       })
  
        this.setState({ loading: true, refreshing: false })
      })
      .catch(error => {
        alert("Please check internet connection");
      });
  }
  _scanImage = async () => {
    this.setState({ showPopup: false })
    try {
      let result = await ImagePicker.launchCameraAsync({
        allowsEditing: false,
        quality: 1,
        base64: true
      });
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        this.toggleLoaderModal();
        fetch(URL + "upload_file", {
          method: "POST",
          body: JSON.stringify({
            chat_id: this.state.chat_id,
            user_id: this.state.user_id,
            ip: this.state.ip,
            imageBase64: result.base64,

          }),
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(res => res.json())
          .then(async response => {
            //  ////console.log("Send Image")
            //  ////console.log(response)
            if (response.response == "success") {
              this.toggleLoaderModal();
              Alert.alert("Success", "File sent successfully.", [{ text: "OK" }], {
                cancelable: true
              });
            } else {
              this.toggleLoaderModal();
              Alert.alert("Sorry", response.message, [{ text: "OK" }], {
                cancelable: true
              });

            }
          })
          .catch(error => {
            this.toggleLoaderModal();
            //  ////console.log("Please Check Your Internet Connection");

          });
      }

      //  ////console.log(result);

    } catch (E) {
      alert(e);
    }
  };
  callDepartment = (phone) => {

    Linking.openURL(`tel:${phone}`)
  }
  submitmessage = () => {

    if (this.state.message != "") {
      if(this.state.isEditingCommand){
        this.submit_loanNumber(this.state.commandID, this.state.commandType)
      }else{
        if(this.state.isEditing){
          this.update_message_api()
        }else{
        
          this.socket.emit('sendMessage', this.state.message, () => this.setState({ message: '' }));
          this.send_message_api()
        }
      }

      ////console.log(this.state.message);

    }
  
  }
  submit_loanNumber = (index,type) => {
let val = "";
let key = "";
let obj = {};

if(type == "loanNumber"){
  val = this.state.loan_number
  key = 'loanNumber';
obj = {
  'loanNumber' : val
}
}
if(type == "socialSecurity"){
  val = this.state.isEditingCommand ? this.state.message : this.state.social_number;
  key = 'socialNumber';
  obj = {
    'socialNumber' : val
  }
}
if(type == "phoneNumber"){
  val = this.state.isEditingCommand ? this.state.message : this.state.phone_number
  key = 'phone';
  obj = {
    'phone' : val
  }
}
if(type == "email"){
  val = this.state.isEditingCommand ? this.state.message : this.state.email_number
  key = 'email';
  obj = {
    'email' : val
  }
}
if(type == "dollarAmount"){
  val = this.state.isEditingCommand ? this.state.message : this.state.dollar_number
  key = 'dollarAmount';
  obj = {
    'dollarAmount' : val
  }
}
if(type == "percentage"){
  val = this.state.isEditingCommand ? this.state.message : this.state.percentage_number
  key = 'percentage';
  obj = {
    'percentage' : val
  }
}
if(type == "password"){
  val = this.state.isEditingCommand ? this.state.message : this.state.password_number
  key = 'password';
  obj = {
    'password' : val
  }
}
console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
console.log(obj);
console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    fetch("https://serene-river-38311.herokuapp.com/api/user/update/" + this.state.user_id, {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify(obj)

    })


      .then(res => res.json())
      .then(async response => {
        ////console.log("-----------------this.state.loan_number-------------------------")
        ////console.log(this.state.loan_number)
        ////console.log("-----------------this.state.loan_number----------------------")

       console.log(">>>>>User Loan Number>>>>");
       console.log(response);
        if (response) {
          this.setState({ isEditing: false,isEditingCommand: false, message:"",commandID:"", commandType:"" })
          this.getMessages(this.state.user_id);
        }
        let current_obj = this.state.messages[index]
        if (current_obj) {
          if(type == "loanNumber"){
            current_obj.loanNumber = this.state.loan_number
          }
          if(type == "socialSecurity"){
            current_obj.socialSecurity = this.state.social_number
          }
          if(type == "phoneNumber"){
            current_obj.phoneNumber = this.state.phone_number
          }
          if(type == "email"){
            current_obj.email = this.state.email_number
          }
          if(type == "dollarAmount"){
            current_obj.dollarAmount = this.state.dollar_number
          }
          if(type == "percentage"){
            current_obj.percentage = this.state.percentage_number
          }
          if(type == "password"){
            current_obj.password = this.state.password_number
          }
        
          current_obj.enabled = true
        }
        // this.setState({ loan_number: "" })
        ////console.log("this.state.messages[index]")
        ////console.log(this.state.messages[index])
        // AsyncStorage.getItem("userID").then(user_data => {
        //   const val = JSON.parse(user_data);

        //   if (val) {
        //     AsyncStorage.setItem("userID", JSON.stringify(response.user_info));

        //   }
        // })

      })
    // .catch(error => {
    //   alert("Please check internet connection");
    // });

  }
  submit_phoneNumber = () => {
    fetch("https://serene-river-38311.herokuapp.com/api/user/update/" + this.state.user_id, {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        phone: this.state.phone

      })

    })
      .then(res => res.json())
      .then(async response => {
        ////console.log(">>>>>User Phone Number>>>>");
        ////console.log(response);
        if (response) {
          this.setState({ PhoneModalVisible: false })
        }
        // AsyncStorage.getItem("userID").then(user_data => {
        //   const val = JSON.parse(user_data);

        //   if (val) {
        //     AsyncStorage.setItem("userID", JSON.stringify(response.user_info));

        //   }
        // })

      })
    // .catch(error => {
    //   alert("Please check internet connection");
    // });

  }

  submit_emailAddress = () => {
    fetch("https://serene-river-38311.herokuapp.com/api/user/update/" + this.state.user_id, {
      method: "Post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: this.state.email

      })

    })
      .then(res => res.json())
      .then(async response => {
        ////console.log(">>>>>User Email Address>>>>");
        ////console.log(response);
        if (response) {
          this.setState({ EmailModalVisible: false })
        }
        // AsyncStorage.getItem("userID").then(user_data => {
        //   const val = JSON.parse(user_data);

        //   if (val) {
        //     AsyncStorage.setItem("userID", JSON.stringify(response.user_info));

        //   }
        // })

      })
    // .catch(error => {
    //   alert("Please check internet connection");
    // });

  }

  send_message_api = () => {
    ////console.log("https://serene-river-38311.herokuapp.com/api/user/show/"+this.state.user_id)

    fetch("https://serene-river-38311.herokuapp.com/api/user/show/" + this.state.user_id, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        ////console.log("First")
        ////console.log(response)
        if (response.data.joinedWith) {
          fetch(URL + "chat", {
            method: "POST",
            body: JSON.stringify({
              "from": this.state.user_id,
              "to": response.data.joinedWith,
              "body": this.state.message
            }),
            headers: {
              "Content-Type": "application/json"
            }
          })
            .then(res => res.json())
            .then(async response => {
              ////console.log("Second")
              ////console.log(response)
              if (response.data) {
                this.setState({ message: '' })
              } else {
                Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
                  cancelable: true
                });
                this.setState({ isLoading: false });
              }
            })
            .catch(error => {
              this.setState({ isLoading: false });
            });
        } else {
          Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });





  }
  postChatImage = (data) => {
    ////console.log("https://serene-river-38311.herokuapp.com/api/user/show/"+this.state.user_id)
 const photo = {
      uri: data.uri,
      type: "image/jpeg",
      name: "photo"+this.state.user_id+".jpeg",
    };

    
    fetch("https://serene-river-38311.herokuapp.com/api/user/show/" + this.state.user_id, {
      method: "GET",
     
    })
      .then(res => res.json())
      .then(async response => {
        ////console.log("First")
        ////console.log(response)
        if (response.data.joinedWith) {
          const form = new FormData();

    form.append("from", this.state.user_id);
    form.append("to", response.data.joinedWith);
    form.append("body", this.state.message);
    form.append("file", photo);
          fetch(URL + "chat", {
            method: "POST",
            body: form,
          })
            .then(res => res.json())
            .then(async response => {
              console.log("Second")
              console.log(response)
              if (response.data) {
                this.setState({ message: '', showPopup: false })
              } else {
                Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
                  cancelable: true
                });
                this.setState({ isLoading: false });
              }
            })
            .catch(error => {
              this.setState({ isLoading: false });
            });
        } else {
          Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });





  }
  update_message_api = () => {
   //console.log(URL + "chat/"+this.state.current_message_data._id)
   //console.log("-----------this.state.current_message_data------------")
   //console.log(this.state.current_message_data)
   //console.log("-----------this.state.current_message_data------------")

    fetch(URL+"chat/"+this.state.current_message_data._id, {
      method: "PUT",
      body: JSON.stringify({
        "from": this.state.current_message_data.from,
        "to": this.state.current_message_data.to,
        "body": this.state.message
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        ////console.log("Second")
        ////console.log(response)
        if (response.data) {
          this.setState({ message: '', isEditing:false, current_item_obj:[] })

        } else {
          Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });





  }
  update_command_api = () => {
   //console.log(URL + "chat/"+this.state.current_message_data._id)
   //console.log("-----------this.state.current_message_data------------")
   //console.log(this.state.current_message_data)
   //console.log("-----------this.state.current_message_data------------")

    fetch(URL+"chat/"+this.state.current_message_data._id, {
      method: "PUT",
      body: JSON.stringify({
        "from": this.state.current_message_data.from,
        "to": this.state.current_message_data.to,
        "body": this.state.message
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(async response => {
        ////console.log("Second")
        ////console.log(response)
        if (response.data) {
          this.setState({ message: '', isEditingCommand:false, current_item_obj:[] })

        } else {
          Alert.alert("Sorry", "No Operator has joined yet", [{ text: "OK" }], {
            cancelable: true
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });





  }
  exitChat = () => {
    Alert.alert(
      'Confirmation',
      'Are you sure you want to end this chat?',
      [
        {
          text: 'Yes', onPress: () => {
            AsyncStorage.clear();
            AsyncStorage.removeItem('userID');
            AsyncStorage.removeItem('user_image');
            this.props.navigation.push("Login");
          }
        },
        {
          text: 'Cancel',
          onPress: () =>console.log('Cancel Pressed'),
          style: 'cancel',
        }

      ],
      { cancelable: false },
    );
  }

  _keyboardDidShow = e => {
    var KeyboardHeight = e.endCoordinates.height;
    this.setState({
      keyboardHeight: KeyboardHeight,
    });
  };
  _keyboardDidHide = e => {
    this.setState({
      keyboardHeight: 0,
    });
  };
  onFinish = (values) => {


    ////console.log('values', values)
    if (values.loanNumber) {

      setOpenLoanNumberModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { loanNumber: values.loanNumber } }))
    }

    if (values.phoneNumber) {

      setOpenPhoneNumberModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { phone: values.phoneNumber } }))
    }

    if (values.email) {

      setOpenEmailModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { email: values.email } }))
    }


  };
  getPermissionAsync = async () => {
    if (Platform.OS !== 'web') {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
        }
    }
};
 
  pickImage = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 1,
    });

   //console.log("-----++++++++++++++++++________");
   //console.log(result);
   //console.log("-----++++++++++++++++++________");

    if (!result.cancelled) {
      this.setState({user_avatar:result.uri})
      AsyncStorage.setItem("user_image", JSON.stringify(result.uri));
      // setImage(result.uri);
     console.log("__++_++__++__++__++__++")
     console.log("IMAGE UPLOADING")
     console.log("__++_++__++__++__++__++")
      this.postAvatarImage(result);
    }
  };
  _pickImageChat = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 1,
    });

   //console.log("-----++++++++++++++++++________");
   //console.log(result);
   //console.log("-----++++++++++++++++++________");

    if (!result.cancelled) {
   
     console.log("__++_++__++__++__++__++")
     console.log("IMAGE UPLOADING")
     console.log("__++_++__++__++__++__++")
      this.postChatImage(result);
    }
  };

  editMessage = (id) => {
    let messages_data = this.state.messages
    let current_item_obj = this.state.messages.find(
      item => item._id == id
    );

   //console.log("-------OBJ----------")
   //console.log(current_item_obj)
   //console.log("-------OBJ----------")

    current_item_obj.editable = true;
    
    this.setState({message:current_item_obj.body, current_message_data:current_item_obj,  isEditing:true})


  }
  editRequest = (id) => {
    let messages_data = this.state.messages
    let current_item_obj = this.state.messages.find(
      item => item._id == id
    );

   //console.log("-------OBJ----------")
   //console.log(current_item_obj)
   //console.log("-------OBJ----------")

    current_item_obj.editable = true;
    
    this.setState({message
      :current_item_obj.body, current_message_data:current_item_obj,  isEditing:true})


  }
  editCommandMessage = (id,type) => {
    // alert(current_item_obj.toObj[0].type)
    let messages_data = this.state.messages
    let current_item_obj = this.state.messages.find(
      item => item._id == id
    );

   console.log("-------OBJ----------")
   console.log(type)
   console.log("-------OBJ----------")

    current_item_obj.editable = true;
    let value = "";
    if(type == "loanNumber"){
      value=current_item_obj.toObj[0].loanNumber;
    }
    if(type == "socialSecurity"){
      value=current_item_obj.toObj[0].socialNumber;
    }
    if(type == "phoneNumber"){
      value=current_item_obj.toObj[0].phone;
    }
    if(type == "email"){
      value=current_item_obj.toObj[0].email;
    }
    if(type == "dollarAmount"){
      value=current_item_obj.toObj[0].dollarAmount;
    }
    if(type == "percentage"){
      value=current_item_obj.toObj[0].percentage;
    }
    if(type == "password"){
      value=current_item_obj.toObj[0].password;
    }
    console.log("value")
    console.log(value)
    this.setState({message
      :value, current_message_data:current_item_obj,  isEditingCommand:true, commandID:id,commandType:type})


  }
  avatar = () => {
    fetch("https://serene-river-38311.herokuapp.com/api/avatar/list/Enabled" , {
      method: "Get",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(res => res.json())
      .then(async response => {
       //console.log(">>>>>>>>>>>>>>>>>>avatar>>>>>>>>>>>>>>>>");
       //console.log(response);
        if (response.data.length>0) {
          this.setState({ avatar: response.data[0].avatar_url })
  
        }
        else {
          Alert.alert(
            "Sorry",
            response.message,
            [
              {
                text: "Cancel",
                style: "cancel"
              },
              { text: "OK" }
            ],
            { cancelable: false }
          );
        }
  
        this.setState({ loading: true, refreshing: false })
      })
      .catch(error => {
        alert("Please check internet connection");
      });
  }
  render() {
    const { navigation } = this.props.navigation;
    const fontColor = "#82a601";
    let image_path = this.state.user_image;
    let image_uri = "";
    image_uri = this.state.user_image

    //  if(image_uri){
    //   let ispresant=image_uri.indexOf("http");
    //   ////console.log("-------1321313213--------")
    //   ////console.log(ispresant)
    //   if(ispresant==-1){
    //     image_path="http://swmcapp.com/"+this.state.user_image;
    //   }else{
    //     image_path=image_uri
    //   }
    //  }
    // ////console.log("final image_path")
    // ////console.log(image_path)
    //console.log("this.state.messages")
    //console.log(this.state.messages)
    return (

      <MenuProvider style={{ width: deviceWidth, height: deviceHeight, backgroundColor: "#000" }}>


      
        <View style={{ width: "100%", flexDirection: "row", borderColor: "#DBDBDB", backgroundColor: "#111019", paddingTop: 30, alignItems: "center", alignContent: "center" }}>
          <StatusBar style style="light" />
          {/* <TouchableOpacity style={{ width: "10%", marginTop: 10, paddingLeft: 10 }} onPress={() => this.props.navigation.push('profile')}>
            <Entypo name="home" size={20} color="#fff" />
          </TouchableOpacity> */}
          <View style={{ width: "80%", paddingBottom: 10, flexDirection: "row", marginRight: 15, marginTop: 6, paddingLeft: 10 }}>

            <TouchableOpacity style={{ width: "12%", height: 20 }} onPress={()=>this.pickImage()}>
            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
            </TouchableOpacity>
            {/*         
              <View style={{ width: "19%", alignItems: "center", height: 50, borderRadius: 50, borderWidth: 2, borderColor: "#fff" }}>
              </View> */}

            <View style={{ alignItems: "center", paddingTop: 10, }}>
              <Text style={{ fontSize: 16, color: "#71b1f0", fontWeight: "bold" ,fontSize:10 }}> {this.state.user_name}</Text>
            </View>
          </View>

          <TouchableOpacity style={{ width: "20%", alignItems: "center" }}>

            <Menu >
              <MenuTrigger style={{ marginTop: 10 }} >
                <MaterialIcons name="more-horiz" size={20} color="#fff" />
              </MenuTrigger>
              <MenuOptions>
            <MenuOption onSelect={() => {
              AsyncStorage.setItem("screenDesign", JSON.stringify("modern"));
              this.props.navigation.push('Chat')
              }}  >
                  <Text style={{ color: '#003cff', fontSize: 17, fontWeight: "bold" }}>Modern</Text>
                </MenuOption>
            <MenuOption onSelect={() => {
              AsyncStorage.clear();
              this.props.navigation.push('Login')
              }}  >
                  <Text style={{ color: '#003cff', fontSize: 17, fontWeight: "bold" }}>Logout</Text>
                </MenuOption>
           
            </MenuOptions>
            </Menu>
          </TouchableOpacity>
        </View>
        <LinearGradient style={{ width: "100%", height: "90%", borderTopLeftRadius: 30, }} colors={['#01000E', '#01000E']}>
          <ScrollView style={{ borderTopLeftRadius: 30, }} ref={ref => this.scrollView = ref} onContentSizeChange={(contentWidth, contentHeight) => {
            this.scrollView.scrollToEnd({ animated: true });
          }}
            showsVerticalScrollIndicator={false}
          >
            <View style={{ paddingBottom: 30, }}>

              {
                this.state.messages.map((item, index) => {

                  if (item.from != this.state.user_id.toLowerCase()) {

                    
                    if (item.body == "Loan number Request Sent...") { 
                      if(item.toObj){
                        if(!item.toObj[0].lastLoanNumber){
                          return (
                        <View key={index} >
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10 }}>
                            <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0",fontSize:10}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#71b1f0",fontSize:13,fontWeight:"bold" ,fontSize:10}}>
                               Enter your Loan Number
                                </Text>
                              </View>
                              <View style={{paddingTop:7,flexDirection:"row"}}>
                              <View style={{width:"70%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:3}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{  }}>
                                <TextInput
                                returnKeyType={"send"}
                                onFocus={() => this.setState({hideInput : true})}
                                onBlur={() => this.setState({hideInput : false})}
                                onSubmitEditing={() => this.submit_loanNumber(index,'loanNumber')}
                                  keyboardType="number-pad"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 13, paddingHorizontal: 10,paddingTop:3 }}
                                  placeholder={"Enter Loan Number"}
                                  placeholderTextColor="#71b1f0"
                                  onChangeText={(loan_number) => {
                                    this.setState({ loan_number: loan_number });
                                  }}
                                  value={item.toObj[0].loanNumber ? item.toObj[0].loanNumber : null}
                                />
                                </View>
                  
                                </View>
                       
                              </View>
                              <TouchableOpacity style={{ padding:5,paddingLeft:15 }} onPress={() => this.submit_loanNumber(index,'loanNumber')}>
                              <MaterialIcons name="send" size={15} color="#71b1f0" />
              </TouchableOpacity>
                              </View>
                              <View style={{width:"97%",paddingBottom:5}}>
                             <TouchableOpacity style={{marginLeft:"auto"}} onPress={() => this.editCommandMessage(item._id,'loanNumber')} >
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      

                        </View>
                      
                          

                      )
                        }else{
                          return (
                     <View key={index}>
                     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                     <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                     <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                       <View style={styles.box2}>
       {this.state.user_avatar? 
(<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: this.state.user_avatar }} 
/>)
:
         <Image style={styles.box2} source={require('../assets/images/12.png')} />
}

       </View>
                       </View>
                       <View style={{width:"80%"}}>
                         <View style={{padding:4}}>
                           <Text style={{color:"#71b1f0" ,fontSize:8}}>
                             Edit Loan Number
                           </Text>
                         </View>
                      <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{  }}>
                             <TextInput
                           editable={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             value={item.toObj[0].loanNumber ? item.toObj[0].loanNumber : null}
                           />
                           </View>
                           </View>
                         </View>
                         <View style={{marginHorizontal:6}}>
                         <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                         </View>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{ }}>
                           <TextInput
                           returnKeyType={"send"}
                          //  onSubmitEditing={() => this.submit_loanNumber(index,'loanNumber')}
                           keyboardType="number-pad"
                             key={'input-def'}
                             multiline={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             placeholder={""}
                             placeholderTextColor="#71b1f0"
                             onChangeText={(Edited_loan_number) => {
                               this.setState({ loan_number: Edited_loan_number });
                             }}
                             value={item.toObj[0].loanNumber}

                           />
                           </View>
                           </View>
                         </View>
                      </View>
                       </View>
                     </View>
                     <View style={{ width: "40%" }}>
                       </View>
                   </View>
                   </View>
                 )
                        }
                      }
                     
                    }
                   else if (item.body == "Social Security Request Sent...") { 
                      if(item.toObj){
                 

                        if(!item.toObj[0].lasdtSocialNumber){
                          return (
                        <View key={index} >
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10 }}>
                            <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0",fontSize:10}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#71b1f0",fontSize:13,fontWeight:"bold" ,fontSize:10}}>
                                Enter your Social Security Number
                                </Text>
                              </View>
                              <View style={{paddingTop:7,flexDirection:"row"}}>
                              <View style={{width:"70%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:3}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{  }}>
                                <TextInput
                                editable={item.toObj[0].socialNumber ? false : true}
                                returnKeyType={"send"}
                                onFocus={() => this.setState({hideInput : true})}
                                onBlur={() => this.setState({hideInput : false})}
                                onSubmitEditing={() => this.submit_loanNumber(index,'socialSecurity')}
                                  keyboardType="number-pad"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 13, paddingHorizontal: 10,paddingTop:3 }}
                                  placeholder={"Enter Social #"}
                                  placeholderTextColor="#71b1f0"
                                  onChangeText={(social_number) => {
                                    this.setState({ social_number: social_number });
                                  }}
                                  value={item.toObj[0].socialNumber ? item.toObj[0].socialNumber : null }
                                />
                                </View>
                  
                                </View>
                       
                              </View>
                              <TouchableOpacity style={{ padding:5,paddingLeft:15 }} onPress={() => this.submit_loanNumber(index,'socialSecurity')}>
                              <MaterialIcons name="send" size={15} color="#71b1f0" />
              </TouchableOpacity>
                              </View>
                              <View style={{width:"97%",paddingBottom:5
                              
                              }}>
                              {item.toObj[0].socialNumber ? (
                                <TouchableOpacity style={{marginLeft:"auto"}} onPress={() => this.editCommandMessage(item._id,'socialSecurity')} >
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                              ) : null}
                                 
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      

                        </View>
                      
                          

                      )
                        }else{
                          return (
                     <View key={index}>
                     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                     <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                     <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                       <View style={styles.box2}>
       {this.state.user_avatar? 
(<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: this.state.user_avatar }} 
/>)
:
         <Image style={styles.box2} source={require('../assets/images/12.png')} />
}

       </View>
                       </View>
                       <View style={{width:"80%"}}>
                         <View style={{padding:4}}>
                           <Text style={{color:"#71b1f0" ,fontSize:8}}>
                             Edit Social Security Number
                           </Text>
                         </View>
                      <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{  }}>
                             <TextInput
                           editable={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             value={item.toObj[0].lasdtSocialNumber}
                           />
                           </View>
                           </View>
                         </View>
                         <View style={{marginHorizontal:6}}>
                         <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                         </View>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{ }}>
                           <TextInput
                           returnKeyType={"send"}
                           onSubmitEditing={() => this.submit_loanNumber(index,'socialSecurity')}
                           keyboardType="number-pad"
                             key={'input-def'}
                             multiline={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             placeholder={""}
                             placeholderTextColor="#71b1f0"
                             onChangeText={(Edited_social_number ) => {
                               this.setState({ social_number : Edited_social_number  });
                             }}
                             value={item.toObj[0].socialNumber ? item.toObj[0].socialNumber : null}

                           />
                           </View>
                           </View>
                         </View>
                      </View>
                       </View>
                     </View>
                     <View style={{ width: "40%" }}>
                       </View>
                   </View>
                   </View>
                 )
                        }
                      }
                     
                    }
                    else if (item.body == "Phone Number Request Sent...") { 
                      if(item.toObj){
                 

                        if(!item.toObj[0].lastPhone){
                          return (
                        <View key={index} >
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10 }}>
                            <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0",fontSize:10}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#71b1f0",fontSize:13,fontWeight:"bold" ,fontSize:10}}>
                                Enter your Phone Number
                                </Text>
                              </View>
                              <View style={{paddingTop:7,flexDirection:"row"}}>
                              <View style={{width:"70%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:3}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{  }}>
                                <TextInput
                                returnKeyType={"send"}
                                onFocus={() => this.setState({hideInput : true})}
                                onBlur={() => this.setState({hideInput : false})}
                                onSubmitEditing={() => this.submit_loanNumber(index,'phoneNumber')}
                                  keyboardType="number-pad"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 13, paddingHorizontal: 10,paddingTop:3 }}
                                  placeholder={"Phone Number"}
                                  placeholderTextColor="#71b1f0"
                                  onChangeText={(phone_number) => {
                                    this.setState({ phone_number: phone_number });
                                  }}
                                  value={item.toObj[0].phone ? item.toObj[0].phone : null}
                                />
                                </View>
                  
                                </View>
                       
                              </View>
                              <TouchableOpacity style={{ padding:5,paddingLeft:15 }} onPress={() => this.submit_loanNumber(index,'phoneNumber')}>
                              <MaterialIcons name="send" size={15} color="#71b1f0" />
              </TouchableOpacity>
                              </View>
                              <View style={{width:"97%",paddingBottom:5}}>
                              {item.toObj[0].phone ? (
                                <TouchableOpacity style={{marginLeft:"auto"}} onPress={() => this.editCommandMessage(item._id,'phoneNumber')} >
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                              ) : null }
                                  
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      

                        </View>
                      
                          

                      )
                        }else{
                          return (
                     <View key={index}>
                     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                     <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                     <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                       <View style={styles.box2}>
       {this.state.user_avatar? 
(<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: this.state.user_avatar }} 
/>)
:
         <Image style={styles.box2} source={require('../assets/images/12.png')} />
}

       </View>
                       </View>
                       <View style={{width:"80%"}}>
                         <View style={{padding:4}}>
                           <Text style={{color:"#71b1f0" ,fontSize:8}}>
                           Edit Phone Number
                           </Text>
                         </View>
                      <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{  }}>
                             <TextInput
                           editable={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             value={item.toObj[0].lastPhone}
                           />
                           </View>
                           </View>
                         </View>
                         <View style={{marginHorizontal:6}}>
                         <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                         </View>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{ }}>
                           <TextInput
                           returnKeyType={"send"}
                           onSubmitEditing={() => this.submit_loanNumber(index,'phoneNumber')}
                           keyboardType="number-pad"
                             key={'input-def'}
                             multiline={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             placeholder={""}
                             placeholderTextColor="#71b1f0"
                             onChangeText={(Edited_phone_number ) => {
                               this.setState({ phone_number : Edited_phone_number  });
                             }}
                             value={item.toObj[0].phone ? item.toObj[0].phone : null}

                           />
                           </View>
                           </View>
                         </View>
                      </View>
                       </View>
                     </View>
                     <View style={{ width: "40%" }}>
                       </View>
                   </View>
                   </View>
                 )
                        }
                      }
                     
                    }
                    else if (item.body == "Email Request Sent...") { 
                      if(item.toObj){
                 

                        if(!item.toObj[0].lastEmail){
                          return (
                        <View key={index} >
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10 }}>
                            <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0",fontSize:10}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#71b1f0",fontSize:13,fontWeight:"bold" ,fontSize:10}}>
                                Enter your Email Address
                                </Text>
                              </View>
                              <View style={{paddingTop:7,flexDirection:"row"}}>
                              <View style={{width:"70%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:3}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{  }}>
                                <TextInput
                                returnKeyType={"send"}
                                onFocus={() => this.setState({hideInput : true})}
                                onBlur={() => this.setState({hideInput : false})}
                                onSubmitEditing={() => this.submit_loanNumber(index,'email')}
                                  keyboardType="email-address"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 13, paddingHorizontal: 10,paddingTop:3 }}
                                  placeholder={"Email Address"}
                                  placeholderTextColor="#71b1f0"
                                  onChangeText={(email_number) => {
                                    this.setState({ email_number: email_number });
                                  }}
                                  value={item.toObj[0].email ? item.toObj[0].email : null}
                                />
                                </View>
                  
                                </View>
                       
                              </View>
                              <TouchableOpacity style={{ padding:5,paddingLeft:15 }} onPress={() => this.submit_loanNumber(index,'email')}>
                              <MaterialIcons name="send" size={15} color="#71b1f0" />
              </TouchableOpacity>
                              </View>
                              <View style={{width:"97%",paddingBottom:5}}>
                              {item.toObj[0].email ? (
                                <TouchableOpacity style={{marginLeft:"auto"}} onPress={() => this.editCommandMessage(item._id,'email')} >
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                              ) : null}
                                  
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      

                        </View>
                      
                          

                      )
                        }else{
                          return (
                     <View key={index}>
                     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                     <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                     <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                       <View style={styles.box2}>
       {this.state.user_avatar? 
(<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: this.state.user_avatar }} 
/>)
:
         <Image style={styles.box2} source={require('../assets/images/12.png')} />
}

       </View>
                       </View>
                       <View style={{width:"80%"}}>
                         <View style={{padding:4}}>
                           <Text style={{color:"#71b1f0" ,fontSize:8}}>
                           Edit Email
                           </Text>
                         </View>
                      <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{  }}>
                             <TextInput
                           editable={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             value={item.toObj[0].lastEmail}
                           />
                           </View>
                           </View>
                         </View>
                         <View style={{marginHorizontal:6}}>
                         <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                         </View>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{ }}>
                           <TextInput
                           returnKeyType={"send"}
                           onSubmitEditing={() => this.submit_loanNumber(index,'email')}
                           keyboardType="number-pad"
                             key={'input-def'}
                             multiline={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             placeholder={""}
                             placeholderTextColor="#71b1f0"
                             onChangeText={(Edited_email_number ) => {
                               this.setState({ email_number : Edited_email_number  });
                             }}
                             value={item.toObj[0].email ? item.toObj[0].email : null}

                           />
                           </View>
                           </View>
                         </View>
                      </View>
                       </View>
                     </View>
                     <View style={{ width: "40%" }}>
                       </View>
                   </View>
                   </View>
                 )
                        }
                      }
                     
                    }
                    else if (item.body == "Dollar Amount Request Sent...") { 
                      if(item.toObj){
                 

                        if(!item.toObj[0].lastDollarAmount){
                          return (
                        <View key={index} >
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10 }}>
                            <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0",fontSize:10}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#71b1f0",fontSize:13,fontWeight:"bold" ,fontSize:10}}>
                                Enter your Amount $
                                </Text>
                              </View>
                              <View style={{paddingTop:7,flexDirection:"row"}}>
                              <View style={{width:"70%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:3}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{  }}>
                                <TextInput
                                returnKeyType={"send"}
                                onFocus={() => this.setState({hideInput : true})}
                                onBlur={() => this.setState({hideInput : false})}
                                onSubmitEditing={() => this.submit_loanNumber(index,'dollarAmount')}
                                  keyboardType="number-pad"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 13, paddingHorizontal: 10,paddingTop:3 }}
                                  placeholder={"$ Amount"}
                                  placeholderTextColor="#71b1f0"
                                  onChangeText={(dollar_number) => {
                                    this.setState({ dollar_number: dollar_number });
                                  }}
                                  value={item.toObj[0].dollarAmount ? item.toObj[0].dollarAmount : null}
                                />
                                </View>
                  
                                </View>
                       
                              </View>
                              <TouchableOpacity style={{ padding:5,paddingLeft:15 }} onPress={() => this.submit_loanNumber(index,'dollarAmount')}>
                              <MaterialIcons name="send" size={15} color="#71b1f0" />
              </TouchableOpacity>
                              </View>
                              <View style={{width:"97%",paddingBottom:5}}>
                              {item.toObj[0].dollarAmount ? (
                                <TouchableOpacity style={{marginLeft:"auto"}} onPress={() => this.editCommandMessage(item._id,'dollarAmount')} >
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                              ) : null}
                               
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      

                        </View>
                      
                          

                      )
                        }else{
                          return (
                     <View key={index}>
                     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                     <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                     <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                       <View style={styles.box2}>
       {this.state.user_avatar? 
(<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: this.state.user_avatar }} 
/>)
:
         <Image style={styles.box2} source={require('../assets/images/12.png')} />
}

       </View>
                       </View>
                       <View style={{width:"80%"}}>
                         <View style={{padding:4}}>
                           <Text style={{color:"#71b1f0" ,fontSize:8}}>
                           Edit Amount $
                           </Text>
                         </View>
                      <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{  }}>
                             <TextInput
                           editable={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             value={item.toObj[0].lastDollarAmount}
                           />
                           </View>
                           </View>
                         </View>
                         <View style={{marginHorizontal:6}}>
                         <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                         </View>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{ }}>
                           <TextInput
                           returnKeyType={"send"}
                           onSubmitEditing={() => this.submit_loanNumber(index,'dollarAmount')}
                           keyboardType="number-pad"
                             key={'input-def'}
                             multiline={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             placeholder={""}
                             placeholderTextColor="#71b1f0"
                             onChangeText={(Edited_dollar_number ) => {
                               this.setState({ dollar_number  : Edited_dollar_number  });
                             }}
                             value={item.toObj[0].dollarAmount ? item.toObj[0].dollarAmount : null}

                           />
                           </View>
                           </View>
                         </View>
                      </View>
                       </View>
                     </View>
                     <View style={{ width: "40%" }}>
                       </View>
                   </View>
                   </View>
                 )
                        }
                      }
                     
                    }
                    else if (item.body == "Percenatge Request Sent...") { 
                      if(item.toObj){
                 

                        if(!item.toObj[0].lastPercentage){
                          return (
                        <View key={index} >
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10 }}>
                            <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0",fontSize:10}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#71b1f0",fontSize:13,fontWeight:"bold" ,fontSize:10}}>
                                Enter your Percentage
                                </Text>
                              </View>
                              <View style={{paddingTop:7,flexDirection:"row"}}>
                              <View style={{width:"70%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:3}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{  }}>
                                <TextInput
                                returnKeyType={"send"}
                                onFocus={() => this.setState({hideInput : true})}
                                onBlur={() => this.setState({hideInput : false})}
                                onSubmitEditing={() => this.submit_loanNumber(index,'percentage')}
                                  keyboardType="number-pad"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 13, paddingHorizontal: 10,paddingTop:3 }}
                                  placeholder={"Enter Percentage"}
                                  placeholderTextColor="#71b1f0"
                                  onChangeText={(percentage_number) => {
                                    this.setState({ percentage_number: percentage_number });
                                  }}
                                  value={item.toObj[0].percentage ? item.toObj[0].percentage : null}
                                />
                                </View>
                  
                                </View>
                       
                              </View>
                              <TouchableOpacity style={{ padding:5,paddingLeft:15 }} onPress={() => this.submit_loanNumber(index,'percentage')}>
                              <MaterialIcons name="send" size={15} color="#71b1f0" />
              </TouchableOpacity>
                              </View>
                              <View style={{width:"97%",paddingBottom:5}}>
                              {item.toObj[0].percentage ? (
                                <TouchableOpacity  style={{marginLeft:"auto"}} onPress={() => this.editCommandMessage(item._id,'percentage')} >
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                              ) : null}
                            
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      

                        </View>
                      
                          

                      )
                        }else{
                          return (
                     <View key={index}>
                     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                     <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                     <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                       <View style={styles.box2}>
       {this.state.user_avatar? 
(<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: this.state.user_avatar }} 
/>)
:
         <Image style={styles.box2} source={require('../assets/images/12.png')} />
}

       </View>
                       </View>
                       <View style={{width:"80%"}}>
                         <View style={{padding:4}}>
                           <Text style={{color:"#71b1f0" ,fontSize:8}}>
                           Edit percentage
                           </Text>
                         </View>
                      <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{  }}>
                             <TextInput
                           editable={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             value={item.toObj[0].lastDollarAmount}
                           />
                           </View>
                           </View>
                         </View>
                         <View style={{marginHorizontal:6}}>
                         <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                         </View>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{ }}>
                           <TextInput
                           returnKeyType={"send"}
                           onSubmitEditing={() => this.submit_loanNumber(index,'percentage')}
                           keyboardType="number-pad"
                             key={'input-def'}
                             multiline={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             placeholder={""}
                             placeholderTextColor="#71b1f0"
                             onChangeText={(Edited_percentage_number ) => {
                               this.setState({percentage_number: Edited_percentage_number  });
                             }}
                             value={item.toObj[0].percentage ? item.toObj[0].percentage : null}

                           />
                           </View>
                           </View>
                         </View>
                      </View>
                       </View>
                     </View>
                     <View style={{ width: "40%" }}>
                       </View>
                   </View>
                   </View>
                 )
                        }
                      }
                     
                    }
                    else if (item.body == "Password Request Sent...") { 
                      if(item.toObj){
                 

                        if(!item.toObj[0].lastPassword){
                          return (
                        <View key={index} >
                        <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "70%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10 }}>
                            <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0",fontSize:10}}>
                                  Request
                                </Text>
                              </View>
                              <View style={{paddingLeft:4}}>
                                <Text style={{color:"#71b1f0",fontSize:13,fontWeight:"bold" ,fontSize:10}}>
                                Enter Password
                                </Text>
                              </View>
                              <View style={{paddingTop:7,flexDirection:"row"}}>
                              <View style={{width:"70%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:3}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{  }}>
                                <TextInput
                                returnKeyType={"send"}
                                onFocus={() => this.setState({hideInput : true})}
                                onBlur={() => this.setState({hideInput : false})}
                                onSubmitEditing={() => this.submit_loanNumber(index,'password')}
                                  keyboardType="number-pad"
                                  key={'input-def'}
                                  multiline={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 13, paddingHorizontal: 10,paddingTop:3 }}
                                  placeholder={"Enter Password"}
                                  placeholderTextColor="#71b1f0"
                                  onChangeText={(password_number) => {
                                    this.setState({ password_number: password_number });
                                  }}
                                  value={item.toObj[0].password ? item.toObj[0].password : null}
                                />
                                </View>
                  
                                </View>
                       
                              </View>
                              <TouchableOpacity style={{ padding:5,paddingLeft:15 }} onPress={() => this.submit_loanNumber(index,'password')}>
                              <MaterialIcons name="send" size={15} color="#71b1f0" />
              </TouchableOpacity>
                              </View>
                              <View style={{width:"97%",paddingBottom:5}}>
                              {item.toObj[0].password ? (
                                <TouchableOpacity style={{marginLeft:"auto"}} onPress={() => this.editCommandMessage(item._id,'password')} >
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                              ) : null}
                                 
                              </View>
                             

                          

                            </View>

                        

                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                      

                        </View>
                      
                          

                      )
                        }else{
                          return (
                     <View key={index}>
                     <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                     <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                     <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                       <View style={styles.box2}>
       {this.state.user_avatar? 
(<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: this.state.user_avatar }} 
/>)
:
         <Image style={styles.box2} source={require('../assets/images/12.png')} />
}

       </View>
                       </View>
                       <View style={{width:"80%"}}>
                         <View style={{padding:4}}>
                           <Text style={{color:"#71b1f0" ,fontSize:8}}>
                           Edit Password
                           </Text>
                         </View>
                      <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{  }}>
                             <TextInput
                           editable={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             value={item.toObj[0].lastDollarAmount}
                           />
                           </View>
                           </View>
                         </View>
                         <View style={{marginHorizontal:6}}>
                         <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                         </View>
                         <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                           <View style={{flexDirection:"row"}}>
                           <View style={{paddingLeft:2,paddingTop:5}}>
                           <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                           </View>
                             <View style={{ }}>
                           <TextInput
                           returnKeyType={"send"}
                           onSubmitEditing={() => this.submit_loanNumber(index,'password')}
                           keyboardType="number-pad"
                             key={'input-def'}
                             multiline={false}
                             keyboardAppearance={"dark"}
                             style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                             placeholder={""}
                             placeholderTextColor="#71b1f0"
                             onChangeText={(Edited_password_number ) => {
                               this.setState({password_number: Edited_password_number  });
                             }}
                             value={item.toObj[0].password ? item.toObj[0].password : null}

                           />
                           </View>
                           </View>
                         </View>
                      </View>
                       </View>
                     </View>
                     <View style={{ width: "40%" }}>
                       </View>
                   </View>
                   </View>
                 )
                        }
                      }
                     
                    }

                    else {
                      return (

                        <View key={index} style={{ flexDirection: "row", marginTop: 10, paddingLeft: 20 }}>
                          <View style={{ flexDirection:"row",width: "auto", backgroundColor: "#111019", borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomRightRadius: 10, borderBottomLeftRadius: 3 }}>


                            <View style={{ padding: 20 }}>
                              <Text style={{ lineHeight: 26, color:"#71b1f0" ,fontSize:10 }}>{item.body}</Text>
                            </View>
                           

                          </View>

                        </View>
                      )
                    }


                  }
                   else
                   {
                    if(item.previousBody)
                    {
return(
  <View key={index} >
                          <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                          <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0" ,fontSize:8}}>
                                  Edited
                                </Text>
                              </View>
                           <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                              <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:5}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{paddingTop:5  }}>
                                <TextInput
                                returnKeyType={"send"}
                                editable={false}
                                  key={'input-def'}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                                  placeholderTextColor="#71b1f0"
                                  value={item.previousBody}
                                />
                                </View>
                                </View>
                              </View>
                              <View style={{marginHorizontal:6}}>
                              <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                              </View>
                              <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                                <TouchableOpacity onPress={() => this.editMessage(item._id)} style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:5}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{ paddingTop:5}}>
                                  <TextInput
                                editable={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}

                                  placeholderTextColor="#71b1f0"
                                  value={item.body}
                                />
                                </View>
                                </TouchableOpacity>
                              </View>
                           </View>
                            </View>
                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                        </View>
                    
)
                    }
                    else if(item.lastLoanNumber){
                      return(
  <View key={index} >
                          <View  style={{width:"100%", marginTop: 10, paddingLeft: 20,flexDirection:"row-reverse" }}>
                          <View style={{ width: "90%",flexDirection: "row", backgroundColor: '#111019', borderRadius:10,paddingBottom:15 }}>
                          <View style={{width:"15%",paddingLeft:5,paddingTop:5}}>
                            <View style={styles.box2}>
            {this.state.user_file? 
  (<Image style={{width:"100%",height:"100%",resizeMode:"cover",  borderRadius: 80}} source={{ uri: 'https://serene-river-38311.herokuapp.com/'+this.state.user_file }} 
   />)
   :
              <Image style={styles.box2} source={require('../assets/images/12.png')}  />
   }

            </View>
                            </View>
                            <View style={{width:"80%"}}>
                              <View style={{padding:4}}>
                                <Text style={{color:"#71b1f0" ,fontSize:8}}>
                                  Edited loan number
                                </Text>
                              </View>
                           <View style={{marginTop:5,width:"80%",flexDirection:"row"}}>
                              <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                                <View style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:5}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{paddingTop:5  }}>
                                <TextInput
                                returnKeyType={"send"}
                                editable={false}
                                  key={'input-def'}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}
                                  placeholderTextColor="#71b1f0"
                                  value={item.lastLoanNumber}
                                />
                                </View>
                                </View>
                              </View>
                              <View style={{marginHorizontal:6}}>
                              <MaterialCommunityIcons name="arrow-right-bold" size={20} color="#e5b60b" />
                              </View>
                              <View style={{width:"50%",backgroundColor:"#211f31",borderRadius:5}}>
                                <TouchableOpacity onPress={() => this.editRequest(item._id)} style={{flexDirection:"row"}}>
                                <View style={{paddingLeft:2,paddingTop:5}}>
                                <AntDesign name="checkcircle" size={15} color="#71b1f0" />
                                </View>
                                  <View style={{ paddingTop:5}}>
                                  <TextInput
                                editable={false}
                                  keyboardAppearance={"dark"}
                                  style={{ color: '#71b1f0', fontSize: 10,paddingLeft:5 }}

                                  placeholderTextColor="#71b1f0"
                                  value={item.toObj[0].loanNumber}
                                />
                                </View>
                                </TouchableOpacity>
                              </View>
                           </View>
                            </View>
                          </View>
                          <View style={{ width: "40%" }}>
                            </View>
                        </View>
                        </View>
                      )
                    }
                    
                    else{
if(!item.file){
  return (
                      <View key={index} >
                        <View style={{ flexDirection: "row-reverse", marginTop: 10, paddingHorizontal: 20 }}>
                          <View style={{ flexDirection:"row",width: "auto", backgroundColor: "#1E1E12", borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 3 }}>
                            <View style={{ padding: 10 }}>
                          
                              <Text style={{ lineHeight: 26, paddingHorizontal: 10,color:"#71b1f0" ,fontSize:10}}>{item.body}</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.editMessage(item._id)} style={{paddingTop:10,paddingRight:5}}>
                            <AntDesign name="edit" size={15} color="#fff" />
                            
                            </TouchableOpacity>
                          </View>
                          <View style={{ width: "40%" }}>
                          </View>
                        </View>

                      </View>

                    )
}else{
  console.log("item.file");
  console.log(item.file);
  return (
                      <View key={index} >
                        <View style={{ flexDirection: "row-reverse", marginTop: 10, paddingHorizontal: 20 }}>
                          <View style={{ flexDirection:"row",width: "auto", backgroundColor: "#1E1E12", borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 3 }}>
                            <View style={{ padding: 10 }}>
                             <Image source={{uri:"https://serene-river-38311.herokuapp.com/"+item.file}} style={{width:150, height:150, resizeMode:"cover"}}/>
                            </View>
                           
                          </View>
                          <View style={{ width: "40%" }}>
                          </View>
                        </View>

                      </View>

                    )
}
                  
                    }
                     
                   
                  }

                }


                )}

            </View>
          </ScrollView>
          {/* <View style={{ width: "100%", flexDirection: "row", marginBottom: this.state.keyboardHeight ,paddingBottom:40 }}> */}
         
          <View style={{ width: "100%", justifyContent: "center", flexDirection: "row", marginBottom: Constants.platform.ios ? this.state.keyboardHeight : 0, paddingBottom: 65 }}>
          {!this.state.hideInput ? (
            
            <View style={{ width: "88%", backgroundColor: "#383948", borderRadius: 10, flexDirection: "row", paddingVertical: 8 }}>
            {/* <View style={{position:"absolute"}}>
                          
                              </View> */}
              <View style={{ width: "80%" }}>
              {this.state.typing ? (
                <Text style={{ color: '#fff', fontSize: 10,paddingLeft:15}}>
                           typing . . .
                              </Text>
              ) : null}
              
                <TextInput
                      ref ={ref => this.inputText = ref}
                  multiline={true}
                  keyboardAppearance={"dark"}
                  style={{ color: '#fff', fontSize: 15, paddingHorizontal: 15 }}
                  placeholder={"Type your message here..."}
                  placeholderTextColor="#AEAEAE"
                  onChangeText={(search) => {
                    this.setState({ message: search });
                  }}
                  value={this.state.message}
                />
              </View>
              <TouchableOpacity style={{ width: "10%", marginTop: 12 }} onPress={() => this.setState({ showPopup: true })}>
                <Entypo name="attachment" size={20} color="#71b1f0" />
              </TouchableOpacity>

              <TouchableOpacity style={{ width: "10%", marginTop: 6 }} onPress={() => this.submitmessage()}>
                <View style={{ width: "70%", height: 20 }}>
                  <Image style={styles.box1} source={require('../assets/images/admin_av.png')} />
                </View>
              </TouchableOpacity>

            </View>
          ) : null}
         
            {/* <TouchableOpacity style={{ width: "12%", marginTop: 10, marginHorizontal: 5, borderRadius: 100, borderColor: "black" }} onPress={() => {
              if (this.state.message == "") {
                this.setState({ show_loan: !this.state.show_loan })
              }

            }}>
              <MaterialCommunityIcons name="dlna" size={24} color="#C7C7C7" />
            </TouchableOpacity> */}

            {/* <TouchableOpacity style={{width:"20%"}}>
               <Ionicons style={{}} name="md-add" size={24} color="#ffffff" />
               </TouchableOpacity>
               <TouchableOpacity style={{width:"20%"}}onPress={()=>this.sendMessage()}>
               <MaterialIcons style={{marginHorizontal:30}} name="send" size={24} color="#fff" />
               </TouchableOpacity>
          */}

            {this.state.showPopup ? (
              <View style={{ width: deviceWidth, backgroundColor: "#fff", borderTopLeftRadius: 30, borderTopRightRadius: 30, borderTopColor: "#80080", position: "absolute", bottom: 80 }}>
                {/* <TouchableOpacity onPress={this._pickImageChat} style={{ width: "100%", alignContent: "center", alignItems: "center", borderBottomColor: "#808080", borderBottomWidth: 0.8, paddingVertical: 10 }}>
                  <Text style={{ fontSize: 22 }}>Gallery</Text>
                </TouchableOpacity> */}
                <TouchableOpacity onPress={this._pickImageChat} style={{ width: "100%", alignContent: "center", alignItems: "center", borderBottomColor: "#808080", borderBottomWidth: 0.8, paddingVertical: 10 }}>
                  <Text style={{ fontSize: 22 }}>Take picture</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({ showPopup: false })} style={{ width: "100%", alignContent: "center", alignItems: "center", borderBottomColor: "#808080", borderBottomWidth: 0.8, paddingVertical: 10 }}>
                  <Text style={{ fontSize: 22, color: "#ff0000" }}>Cancel</Text>
                </TouchableOpacity>
              </View>
            ) : null}

            {/* <View style={{ width: "100%", paddingVertical: 10, backgroundColor: "#fff" }}>
                <View style={{ width: "100%" }}>
                  <Text style={{ fontSize: 18, fontWeight: "bold", textAlign: "center" }}>LOAN NUMBER</Text>
                </View>
                <View style={{ width: "100%", alignContent: "center", alignItems: "center", marginTop: 10 }}>
                  <View style={{ width: "88%", backgroundColor: "#FFf", borderRadius: 50, flexDirection: "row", marginLeft: 8, paddingVertical: 8, borderColor: "#e9e9e9", borderWidth: 0.8 }}>
                    <View style={{ width: "90%" }}>
                      <TextInput
                        key={'input-num'}
                        multiline={false}
                        keyboardType={"number-pad"}
                        style={{ color: "black", fontSize: 15, paddingHorizontal: 15 }}
                        placeholder={"Please enter Loan Number"}
                        placeholderTextColor="#AEAEAE"
                        onChangeText={(search) => {
                          this.setState({ loan_number: search });
                        }}
                        value={this.state.loan_number}
                      />
                    </View>
                    <TouchableOpacity style={{ width: "10%", marginTop: 6 }} onPress={() => this.submit_loanNumber()}>
                      <MaterialIcons style={{}} name="send" size={20} color="#C7C7C7" />
                    </TouchableOpacity>

                  </View>

                </View>

              </View> */}
            
              <Modal isVisible={this.state.loginModal}>
              <View style={{width:"100%", backgroundColor:"#fff", alignItems:"center", alignContent:"center"}}>
              <View style={{width:"100%", paddingVertical:10}}>
                <Text style={{fontSize:18, fontWeight:"bold", textAlign:"center"}}>How may I call you ?</Text>
              </View>
              <View style={{width:"90%", paddingVertical:10}}>
              <View style={{width:"100%", borderRadius:10, borderWidth:0.8, borderColor:"#808080", paddingVertical:5}}>
              <TextInput
                        key={'input-num'}
                        multiline={false}
                        keyboardType={"default"}
                        style={{ color: "black", fontSize: 15, paddingHorizontal: 15 }}
                        placeholder={"Please enter Full Name"}
                        placeholderTextColor="#AEAEAE"
                        onChangeText={(full_name) => {
                          this.setState({ full_name: full_name });
                        }}
                        value={this.state.full_name}
                      />
              </View>
                       <Text style={{ color: "red" }}>{this.state.name_err}</Text>
              
              </View>
               

            <TouchableOpacity onPress={() => this.submit()} style={{width:"60%", backgroundColor:"#000", marginVertical:20, borderRadius:10, paddingVertical:15}}>
<Text style={{textAlign:"center", color:"#fff"}}>Submit</Text>
            </TouchableOpacity>
          </View>
      </Modal>
            <Modal isVisible={this.state.LoanModalVisible}>
              <View style={{ width: "100%", flexDirection: "row", marginBottom: Constants.platform.ios ? this.state.keyboardHeight : 0, marginLeft: 10, marginTop: 10 }}>
                <View style={{ width: "70%", backgroundColor: "#3939c8", borderRadius: 10, flexDirection: "row", paddingLeft: 15, paddingVertical: 8, justifyContent: "space-between" }}>
                  <View style={{ width: "80%", backgroundColor: "#FFf", paddingLeft: 10 }}>
                    <TextInput
                      keyboardType="name-phone-pad"
                      key={'input-def'}
                      multiline={true}
                      keyboardAppearance={"dark"}
                      style={{ color: 'black', fontSize: 15, paddingHorizontal: 10 }}
                      placeholder={"Loan Number..."}
                      placeholderTextColor="#AEAEAE"
                      onChangeText={(loan_num) => {
                        this.setState({ message1: loan_num });
                      }}
                      value={this.state.message1}
                    />
                  </View>

                  <View style={{ width: "10%", alignItems: "center", backgroundColor: "#FFf", marginRight: 15, paddingVertical: 8 }}>
                    <TouchableOpacity style={{ width: "90%", paddingLeft: 3 }} onPress={() => this.submit_loanNumber()}>
                      <FontAwesome name="send-o" size={15} color="#bbbbbb" />
                    </TouchableOpacity>
                  </View>


                </View>
              </View>
            </Modal>

            <Modal isVisible={this.state.PhoneModalVisible}>
              <View style={{ width: "100%", paddingVertical: 10, backgroundColor: "#fff" }}>
                <View style={{ width: "100%" }}>
                  <Text style={{ fontSize: 18, fontWeight: "bold", textAlign: "center" }}>Phone Number</Text>
                </View>
                <View style={{ width: "100%", alignContent: "center", alignItems: "center", marginTop: 10 }}>
                  <View style={{ width: "88%", backgroundColor: "#FFf", borderRadius: 50, flexDirection: "row", marginLeft: 8, paddingVertical: 8, borderColor: "#e9e9e9", borderWidth: 0.8 }}>
                    <View style={{ width: "90%" }}>
                      <TextInput
                        key={'input-num'}
                        multiline={false}
                        keyboardType={"phone-pad"}
                        style={{ color: "black", fontSize: 15, paddingHorizontal: 15 }}
                        placeholder={"Please enter Contact Number"}
                        placeholderTextColor="#AEAEAE"
                        onChangeText={(search) => {
                          this.setState({ phone: search });
                        }}
                        value={this.state.phone}
                      />
                    </View>
                    <TouchableOpacity style={{ width: "10%", marginTop: 6 }} onPress={() => this.submit_phoneNumber()}>
                      <MaterialIcons style={{}} name="send" size={20} color="#C7C7C7" />
                    </TouchableOpacity>

                  </View>

                </View>

              </View>
            </Modal>
            <Modal isVisible={this.state.EmailModalVisible}>
              <View style={{ width: "100%", paddingVertical: 10, backgroundColor: "#fff" }}>
                <View style={{ width: "100%" }}>
                  <Text style={{ fontSize: 18, fontWeight: "bold", textAlign: "center" }}>Email</Text>
                </View>
                <View style={{ width: "100%", alignContent: "center", alignItems: "center", marginTop: 10 }}>
                  <View style={{ width: "88%", backgroundColor: "#FFf", borderRadius: 50, flexDirection: "row", marginLeft: 8, paddingVertical: 8, borderColor: "#e9e9e9", borderWidth: 0.8 }}>
                    <View style={{ width: "90%" }}>
                      <TextInput
                        key={'input-num'}
                        multiline={false}
                        // keyboardType={"phone-pad"}
                        style={{ color: "black", fontSize: 15, paddingHorizontal: 15 }}
                        placeholder={"Please enter your E-mail address"}
                        placeholderTextColor="#AEAEAE"
                        onChangeText={(search) => {
                          this.setState({ email: search });
                        }}
                        value={this.state.email}
                      />
                    </View>
                    <TouchableOpacity style={{ width: "10%", marginTop: 6 }} onPress={() => this.submit_emailAddress()}>
                      <MaterialIcons style={{}} name="send" size={20} color="#C7C7C7" />
                    </TouchableOpacity>

                  </View>

                </View>

              </View>
            </Modal>


            {/* <Footer1 title={"Chat"} navigation={this.props.navigation} /> */}

          </View>


        </LinearGradient>


      </MenuProvider>
    )




  }

}
const styles = StyleSheet.create({

  chatbox: {

    width: "100%",
    marginLeft: 90,
    marginBottom: 50,

    paddingVertical: 15,

    backgroundColor: "#22c0f0",
    borderRadius: 50,
  },
  box2: {

    width: "85%",
    height: 30,
    resizeMode: "cover",
    borderRadius: 80
    // , borderWidth: 1, borderColor: "#f7bb97"



  },
  box1: {

    width: "95%",
    height: 30,
    resizeMode: "contain",

    // , borderWidth: 1, borderColor: "#f7bb97"



  },

})

// {this.state.user_image ? (
//   <View style={{ width: "19%", borderRadius: 50, borderWidth: 1, borderColor: "#fff", height: 50 }}>
//     {/* <Image style={{width:40,height:40,borderRadius:100}} source={require('../assets/profile.png')} /> */}
//     <Image style={styles.box} source={{ uri: this.state.user_image }} />
//   </View>
// ) : (
//   <View style={{ width: "19%", alignItems: "center", height: 50, borderRadius: 50, borderWidth: 2, borderColor: "#fff" }}>
//   </View>
// )}