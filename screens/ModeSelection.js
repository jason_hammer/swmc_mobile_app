//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
  image
} from "react-native";
console.disableYellowBox = true;
import Footer from './Footer';
import { LinearGradient } from 'expo-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons';
import Constants from "expo-constants";
import { Container, Picker, Content, Button } from "native-base";
import { URL, SWMC_ADMIN, SWMC_USER } from "../components/API";
import BaseHeader from "../components/BaseHeader";
import publicIP from 'react-native-public-ip';
import * as ImagePicker from 'expo-image-picker';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign } from '@expo/vector-icons';

import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
import { TextInput } from "react-native-gesture-handler";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class WelcomeNoteRetro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      name_err: "",
      address_err: "",
      department_err: "",
      email: "",
      email_err: "",
      department: 0,
      pointer: 0,
      ip: "",
      images: [],
      user_avatar: "",
      departments_data: [],
      isLoading: false,
      loader: true,
    };
  }
 

 
 
 
  render() {


    return (


      <LinearGradient style={{ width: screenWidth, height: screenHeight }} colors={['#fff', '#fff']}>
        <KeyboardAwareScrollView
          contentContainerStyle={{ width: screenWidth, height: screenHeight }}
        >
          {/* <ImageBackground source={require("../assets/images/bcgrnd.png")} imageStyle={{resizeMode:"cover", overlayColor:"grey"}} style={{flex:1 }}> */}


          <View style={{ flex: 1 }}>
            <BaseHeader
              navigation={this.props.navigation}
              title={"Customer Care"}
              logo={true}
            />

           <ImageBackground source={require("../assets/images/bcgrnd.png")} imageStyle={{resizeMode:"cover", overlayColor:"grey"}} style={{flex:1 }}> 
               <TouchableOpacity  style={{paddingVertical:10,alignItems:"center",marginTop:80}} onPress={() =>{
                 AsyncStorage.setItem("screenDesign", JSON.stringify("modern"));
                 this.props.navigation.navigate("Login")
               }} >
           <LinearGradient style={styles.login_button  } colors={['#f7bb97', '#dd5e89']}>
               <Text style={{ color: '#fff',fontSize:20 }}>Modern</Text>
                </LinearGradient>
                </TouchableOpacity>  
                   <TouchableOpacity  style={{paddingVertical:10,alignItems:"center",marginTop:40}} onPress={() =>{
                 AsyncStorage.setItem("screenDesign", JSON.stringify("retro"));
                 this.props.navigation.push("ChatRetro")
               }} >
           <LinearGradient style={styles.login_button  } colors={['#121c62', '#111019']}>
               <Text style={{ color: '#fff',fontSize:20 }}>Retro</Text>
                </LinearGradient>
                </TouchableOpacity>
           </ImageBackground> 

             </View>   
             




          {/* </View> */}
        
         



        </KeyboardAwareScrollView>

      </LinearGradient>



















    );
  }
}
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  search: {

    width: "100%",
    marginTop: 10,
    paddingVertical: 15,

    backgroundColor: "#F5F5F5",
    borderRadius: 50,
  },
  box: {

    width: "100%",
    height: 60,
    resizeMode: "cover",
    borderRadius: 80



  },
  login_button: {
    width: "50%",
    paddingVertical: 10,
    alignItems: "center",
    alignContent: "center",

    backgroundColor: "#fff",
    borderRadius: 10,
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,

    bottom: 10


  },
})





















































{/* {this.state.isLoading ? (
                 <Button
                   bordered
                   style={{
                     width: "100%",
                     alignItems: "center",
                     justifyContent: "center",
                     borderColor: fontColor,
                     borderRadius: 5,
                     alignItems:"center"
                   }}
                 >
                   <ActivityIndicator color={"#fd7e14"} size={"small"} />
                 </Button>
               ) : (
               
               <TouchableOpacity onPress={()=>this.props.navigation.navigate('ChatDashboard')}>
                   <image source={require('../assets/images/enter.png')}/>
                   </TouchableOpacity>
               )} */}
