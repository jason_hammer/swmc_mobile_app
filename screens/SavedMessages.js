import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, Picker, Dimensions, ImageBackground, TextInput, AsyncStorage, Keyboard, Alert, ScrollView, RefreshControl } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons, MaterialCommunityIcons, Octicons } from '@expo/vector-icons';
import Constants from "expo-constants";
import { Linking } from 'react-native'
import TimeAgo from 'react-native-timeago';
// import { TextField } from "react-native-material-textfield";
import { Container, Input, Content, Button } from "native-base";
import { URL } from "../components/API";
import BaseHeader from "../components/BaseHeader";
import publicIP from 'react-native-public-ip';
import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
// import KeyboardSpacer from 'react-native-keyboard-spacer';
// import Modal from 'react-native-modal';
import * as ImagePicker from 'expo-image-picker';
import Footer from './Footer';
import moment from 'moment';
import { LinearGradient } from 'expo-linear-gradient';

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;



export default class SavedMessages extends Component {
  // const [started, setStarted] = useState(false)
  constructor(props) {
    super(props);
    this.state = {
      user_id:"",
      user_image: "",
      get_chat:[],
      loading: true,
      refreshing: false
 
    };

  }


  componentDidMount() {
    AsyncStorage.getItem("userID").then(user_data => {
      const val = JSON.parse(user_data);
      if (val) {

        this.setState({
      
          user_id: val._id,

        });
      }
       this.getHistory()
    
    });
    AsyncStorage.getItem("user_image").then(image => {
      const val_image = JSON.parse(image);

      if (val_image) {
     
        this.setState({ user_image: val_image
        });
        this.state.user_image = val_image;

      }
    });
  }
getHistory=()=>{
  this.setState({ refreshing: true })
  fetch("https://serene-river-38311.herokuapp.com/api/chat/list/" +  this.state.user_id, {
    method: "Get",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then(res => res.json())
    .then(async response => {
      console.log("getHistory");
      console.log(response);
      if (response.data) {
        this.setState({ get_chat: response.data })

      }
      else {
        Alert.alert(
          "Sorry",
          response.message,
          [
            {
              text: "Cancel",
              style: "cancel"
            },
            { text: "OK" }
          ],
          { cancelable: false }
        );
      }

      this.setState({ loading: true, refreshing: false })
    })
    .catch(error => {
      alert("Please check internet connection");
    });

}





  render() {
  
    return (
      <View style={{ flex: 1, backgroundColor: "#000" }}>
        <View style={{ alignItems: 'center' }}>
          <View style={{ width: "100%", marginTop: 50, paddingBottom: 10, borderBottomColor: "#A3A3A3", flexDirection: "row" }}>


            <View style={{ width: "100%", alignItems: "center" }}>
              <Text style={{ color: "#fff", fontSize: 25, fontWeight: "bold" }}>Messages</Text>
            </View>
          </View>



          <LinearGradient style={{ width: "100%", height: "100%", borderTopRightRadius: 35 }} colors={['#f7bb97', '#dd5e89']}>
          {this.state.loading ? (
            <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.getHistory()} />}>

{this.state.get_chat.length > 0 ?
this.state.get_chat.map((item, index) => {
  return (

  <View key={index} style={{ width: "100%", flexDirection: "row", marginTop: 20, }}>
    <TouchableOpacity style={{ width: "20%", alignItems: "center", borderTopRightRadius: 15 }}>
      <View style={{ width: "90%", paddingLeft: 10 }}>
        <Image style={styles.box} source={{ uri: this.state.user_image }} />
      </View>
    </TouchableOpacity>
    <TouchableOpacity style={{ width: "60%", borderColor: "#DBDBDB", borderBottomWidth: 1, paddingBottom: 2 }}>

      <View style={{ paddingHorizontal: 10, paddingTop: 5 }}>
        <Text style={{ fontSize: 20, color: "#fff", fontWeight: "bold" }}>{item.name}</Text>
      </View>
      <View style={{ paddingTop: 5, paddingHorizontal: 10 }}>
        <Text style={{ fontSize: 14, color: "#fff", fontWeight: "bold" }}>Hey!Wanna Cath up for movie  ?</Text>
      </View>
    </TouchableOpacity>

    <View style={{ width: "20%", alignItems: "center", alignContent: "center", borderColor: "#DBDBDB", borderBottomWidth: 1 }}>
      <View style={{ width: "100%",paddingTop:10 }}>
        <Text style={{ fontSize: 13, color: "#fff" }}> {moment(item.createdAt).format("DD/MM/YY")}</Text>
      </View> 
      <View style={{ width: "100%",paddingTop:10 }}>
        <Text style={{ fontSize: 13, color: "#fff" }}> {moment(item.createdAt).format("h:m A")}</Text>
      </View>
 
    </View>
  </View>

  )
})
: null
}
</ScrollView>

          ):(
            <View style={{ width: "100%", height: 200, alignItems: "center", justifyContent: "center", alignContent: "center" }}>
                <Text>NO Chat Available</Text>
              </View>
          )}



          </LinearGradient>







        </View>

        <Footer title={"Messages"} back={false} navigation={this.props.navigation} />

      </View>






    )




  }

}
const styles = StyleSheet.create({

  search: {

    width: "100%",
    marginTop: 10,
    paddingVertical: 15,

    backgroundColor: "#F5F5F5",
    borderRadius: 50,
  },
  box: {

    width: "100%",
    height: 60,
    resizeMode: "cover",
    borderRadius: 80
    , borderWidth: 3, borderColor: "#fff"


  },
  getstarted_button: {
    width: "40%",
    right: -30,
    paddingVertical: 100,
    paddingHorizontal: 100,
    position: "absolute", zIndex: 100,
    alignItems: "center",
    bottom: 150,



  },
})

// <View style={{width:"100%",marginTop:30}}>
// <View style={styles.getstarted_button} >
// <ActionButton buttonColor="#9b59b6">
//      <ActionButton.Item buttonColor='#9b59b6' title="jkljkljkljljl"  onPress={() => this.exitChat()}>
//        {/* <Icon name="android-create" style={styles.actionButtonIcon} /> */}
//        <SimpleLineIcons name="logout" size={24} color="black" />
//      </ActionButton.Item>
//      <ActionButton.Item buttonColor='#3498db' title="Notifications" onPress={() =>this.props.navigation.navigate("Chat")}>
//      <Entypo name="chat" size={24} color="black" />

//      </ActionButton.Item>
//      <ActionButton.Item buttonColor='#3498db' title="Notifications" onPress={() =>this.props.navigation.navigate("Chat")}>
//      <Entypo name="chat" size={24} color="black" />

//      </ActionButton.Item>


//    </ActionButton>
// </View>
// </View> 