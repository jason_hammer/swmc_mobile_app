import React, { useEffect, useState } from 'react'
import styles from './joinChat.module.css'
import { io } from "socket.io-client";
import { Form, Input, Button, Select, Row, Col, Card, Modal, Image, Badge, Popover } from 'antd';
import { addLoanNumberRequest, getSingleUserChatListRequest, logoutJoinUserRequest, sendMessageFromAdminToUserRequest, showJoinUserRequest } from 'redux/actions';
import { useDispatch, useSelector } from "react-redux";
import ReactSpinner from 'components/reactSpinners'
import { useBeforeunload } from 'react-beforeunload';

import { BASE_URL } from 'Constants/defaultValues';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import Avatar from 'antd/lib/avatar/avatar';
const { Option } = Select;
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};


const socket = io(BASE_URL);
const UserChat = () => {

  const messagesEndRef = React.useRef(null)


  const [joinedUser, setJoinedUser] = useState(null)
  const [popoverState, setPopoverState] = useState({
    visible: false,
    title: null,
    content: null
  })


  const SingleUserChatList = useSelector(state => state.chatReducer.singleUserChatList)
  const loadingSingleUserChatListButton = useSelector(state => state.chatReducer.loadingSingleUserChatListButton)
  const addedJoinUser = useSelector(state => state.joinUserReducer.addedJoinUser)


  const [openLoanNumberModal, setOpenLoanNumberModal] = useState(false)
  const [openPhoneNumberModal, setOpenPhoneNumberModal] = useState(false)
  const [openEmailModal, setOpenEmailModal] = useState(false)
  const [chatItemIds, setChatItemIds] = useState([])
  const [beforeJoinMessages, setBeforeJoinMessages] = useState([])
  const [popover, setPopover] = useState(false)
  const [chatListMessages, setChatListMessages] = useState([])




  useEffect(() => {

    if (SingleUserChatList) {

      setChatListMessages([...SingleUserChatList])
    }


  }, [SingleUserChatList])


  useEffect(() => {
    let chatItemIds = []

    if (SingleUserChatList) {
      SingleUserChatList.map((chatItem, i) => {
        if (chatItem.body.startsWith('Loan number Request Sent...')) {
          return chatItemIds.push(chatItem._id)
        }
      })
    }

    setChatItemIds([...chatItemIds])


  }, [SingleUserChatList])


  useEffect(() => {


    socket.on('messages', (res) => {

      console.log("Messages", res)
      if (localStorage.getItem('join_single_user_id')) {
        console.log("Entered")
        dispatch(getSingleUserChatListRequest(localStorage.getItem('join_single_user_id')))

        return setOpenLoanNumberModal(true)

      }


    })

    socket.on('user_info_type', (res) => {

      console.log("user_info_type", res)
      if (res.id === localStorage.getItem('join_single_user_id')) {

        if (res.type === "loanNumber") {
          setChatItemIds([])
          return setOpenLoanNumberModal(true)
        }
        if (res.type === "phone") {
          return setOpenPhoneNumberModal(true)
        }
        if (res.type === "email") {

          return setOpenEmailModal(true)

        }
      }


    })

    socket.on('endChat', (res) => {

      console.log("endChat", res)

      if (res && res.user && res.user._id === localStorage.getItem('join_single_user_id')) {
        setJoinedUser(res.user)
        if (res.user.joinedWith) {

          return setPopover(true)

        }
        setPopover(false)
      }

    })


    socket.on('onlineUser', (res) => {
      console.log("online user", res)
      if (res && res.user && res.user._id === localStorage.getItem('join_single_user_id')) {
        setJoinedUser(res.user)
        if (res.user.joinedWith) {

          return setPopover(true)

        }
        setPopover(false)
      }


    })

  }, [])




  useEffect(() => {


    if (!localStorage.getItem('join_single_user_id')) {
      return history.push('/join')
    }
    else {
      dispatch(showJoinUserRequest(localStorage.getItem('join_single_user_id')))
      dispatch(getSingleUserChatListRequest(localStorage.getItem('join_single_user_id')))
    }

  }, [])



  const scrollToBottom = () => {

    if (joinedUser) {
      return messagesEndRef.current.scrollIntoView({ behavior: "smooth" })

    }
  }

  useEffect(scrollToBottom, [joinedUser, SingleUserChatList, chatListMessages]);



  // const doSomethingBeforeUnload = async () => {

  // await dispatch(logoutJoinUserRequest(joinedUser._id))


  // }

  // // Setup the `beforeunload` event listener
  // const setupBeforeUnloadListener = () => {
  // window.addEventListener("beforeunload", (ev) => {

  // window.confirm("Yessfds")
  // return doSomethingBeforeUnload();
  // });
  // };


  // const setupCleanerBeforeUnloadListener = () => {
  // window.removeEventListener("beforeunload")
  // };

  // useEffect(() => {
  // setupBeforeUnloadListener();
  // return () => {
  // setupCleanerBeforeUnloadListener()
  // }
  // }, [])



  const dispatch = useDispatch()
  const history = useHistory()
  const [inputMessage, setInputMessage] = useState("")

  useEffect(() => {

    socket.on('join_user', data => {
      console.log("join_user socket", data)
      if (data) {

        return setPopoverState({
          ...popoverState,
          visible: true,
          title: data.message
        })
      }
    })


  }, [])


  useEffect(() => {
    if (joinedUser) {

      dispatch(getSingleUserChatListRequest(joinedUser._id))
    }


  }, [joinedUser])

  useEffect(() => {

    setJoinedUser(addedJoinUser)


  }, [addedJoinUser])




  const onFinish = (values) => {


    console.log('values', values)
    if (values.loanNumber) {

      setOpenLoanNumberModal(false)

      dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { loanNumber: values.loanNumber } }))
      // if (joinedUser && joinedUser.isJoined) {
      // let dispatchedMessage = {
      // to: joinedUser.joinedWith,
      // from: joinedUser._id,
      // body: values.loanNumber
      // }

      // dispatch(sendMessageFromAdminToUserRequest(dispatchedMessage))
      // }
    }

    if (values.phoneNumber) {

      setOpenPhoneNumberModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { phone: values.phoneNumber } }))
    }

    if (values.email) {

      setOpenEmailModal(false)
      return dispatch(addLoanNumberRequest({ id: joinedUser._id, type: { email: values.email } }))
    }


  };

  useEffect(() => {

    if (joinedUser && joinedUser.joinedWith) {

      beforeJoinMessages.forEach((message, i) => {
        let dispatchedMessage = {
          to: joinedUser.joinedWith,
          from: joinedUser._id,
          body: message.body
        }
        dispatch(sendMessageFromAdminToUserRequest(dispatchedMessage))
      })
      return setBeforeJoinMessages([])

    }

  }, [joinedUser])



  const onMessageSent = () => {


    if (joinedUser && joinedUser.isJoined) {
      let dispatchedMessage = {
        to: joinedUser.joinedWith,
        from: joinedUser._id,
        body: inputMessage
      }

      dispatch(sendMessageFromAdminToUserRequest(dispatchedMessage))
      setInputMessage("")


    }
    else {

      setBeforeJoinMessages([...beforeJoinMessages, {
        body: inputMessage,
      }])

      setChatListMessages([...chatListMessages, {
        body: inputMessage,
        id: Math.floor(Math.random() * 100),
        fromObj: [],
        waiting: "Waiting"
      }])

      setInputMessage("")
    }




  }




  const onChatEnd = async () => {

    await dispatch(logoutJoinUserRequest({ data: joinedUser._id, history }))
    setPopover(false)
    return
  }





  return (
    <div className={styles.container}>

      <Row className="w-100">
        <Col span={8}>

        </Col>
        <Col span={8} className="w-100">





          <Modal title="Enter Phone Number" visible={openPhoneNumberModal} closable={false} footer={null} maskClosable={false} >

            <Form {...layout} onFinish={onFinish}>
              <Form.Item
                name="phoneNumber"
                label="Phone Number"
                rules={[
                  {
                    required: true,
                    message: "Please type phone number"
                  },
                ]}
              >
                <Input type="number" />
              </Form.Item>
              <Button type="primary" htmlType="submit">
                Send Phone Number
</Button>
            </Form>

          </Modal>

          <Modal title="Enter Email" visible={openEmailModal} closable={false} footer={null} maskClosable={false} >

            <Form {...layout} onFinish={onFinish}>
              <Form.Item
                name="email"
                label="Email"
                rules={[
                  {
                    required: true,
                    message: "Please type email"
                  },
                ]}
              >
                <Input type="email" />
              </Form.Item>
              <Button type="primary" htmlType="submit">
                Send Email
</Button>
            </Form>

          </Modal>



          <Card width="100%" title={<div className="leading-3">

            <div className="flex items-center mb-3">
              <Badge color={joinedUser && joinedUser.isJoined ? "green" : "red"} dot>

                <Avatar
                  src={<Image src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                />

              </Badge>
              <div className="ml-2">
                {joinedUser && joinedUser.name || "Loading..."}
              </div>

            </div>


            <div className="font-normal" style={{ fontSize: 12 }}>
              {joinedUser && joinedUser.isJoined ? `Joined with ${joinedUser.admin_name}` : "Waiting to be joined ..."}
            </div>
          </div>}>

            <div>
              <div style={{ height: 350, overflowY: 'scroll' }}>
                {chatListMessages && chatListMessages.map((chatItem, index) => {

                  const meUser = chatItem.fromObj[0] ? false : true
                  let meUserName = chatItem.fromObj[0] ? chatItem.fromObj[0].name : joinedUser && joinedUser.name
                  meUserName = chatItem.waiting ? chatItem.waiting : meUserName

                  // let otherUserName = chatItem.toObj[0].name


                  return <div key={index}
                    className={meUser ? "d-flex flex-col w-100 justify-content-end align-items-end mb-3" : "w-auto table mb-3"}>

                    <div className="" style={{ borderRadius: 2, display: 'inline-block', textDecoration: 'underline', color: 'black', fontSize: 11, background: 'white', marginRight: 20, marginBottom: 3, paddingLeft: 10 }}>
                      {(meUserName)}
                    </div>

                    <div style={{ borderRadius: 10, padding: 3, marginRight: 10 }}
                      className={meUser ? `px-3 py-1 ${styles.otherUserMessage}` : `px-3 py-1 ${styles.myMessage}`}

                    >
                      <div>

                        <div>
                          {chatItem.body.startsWith('Loan number Request Sent...') ? <div>

                            <Form onFinish={onFinish}>


                              <Row gutter={8}>
                                <Col span={18}>
                                  <Form.Item
                                    name="loanNumber"
                                    noStyle
                                    rules={[
                                      {
                                        required: true,
                                        message: 'Please type loan number',
                                      },
                                    ]}
                                  >
                                    <Input disabled={chatItem._id !== chatItemIds[chatItemIds.length - 1] || !openLoanNumberModal} placeholder="Type your loan number" />
                                  </Form.Item>
                                </Col>
                                <Col span={6}>
                                  <Button disabled={chatItem._id !== chatItemIds[chatItemIds.length - 1] || !openLoanNumberModal} htmlType="submit" type="primary"><i className="fas fa-paper-plane"></i></Button>
                                </Col>
                              </Row>
                            </Form>

                          </div> : chatItem.body
                          }

                        </div>
                      </div>

                    </div>


                  </div>
                })}


                <div ref={messagesEndRef} />

              </div>


              <div>


                <Input placeholder="Type your message" className="w-100" color="success"
                  value={inputMessage} onChange={(e) => setInputMessage(e.target.value)}
                  onKeyPress={event => event.key === 'Enter' ? onMessageSent() : null}
                />
                <Row justify="end" className="mt-2" gutter={[10, 10]}>
                  <Col>
                    <Button type="primary" onClick={onMessageSent}>Send</Button>
                  </Col>
                  <Col>
                    <Button onClick={onChatEnd}>End Chat</Button>
                  </Col>
                </Row>
              </div>


            </div>
          </Card>


        </Col>

        <Col span={8}>

        </Col>
      </Row>








    </div>
  )
}


export default UserChat