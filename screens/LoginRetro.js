//import React from 'react';
import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ScrollView,
  image
} from "react-native";
console.disableYellowBox = true;
import Footer from './Footer';
import { LinearGradient } from 'expo-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons';
import Constants from "expo-constants";
import { Container, Picker, Content, Button } from "native-base";
import { URL, SWMC_ADMIN, SWMC_USER } from "../components/API";
import BaseHeader1 from "../components/BaseHeader1";
import publicIP from 'react-native-public-ip';
import * as ImagePicker from 'expo-image-picker';
import { MaterialIcons, Ionicons, EvilIcons, MaterialCommunityIcons, Octicons, Feather, Entypo, AntDesign } from '@expo/vector-icons';
import { Font } from 'expo'
import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
import { TextInput } from "react-native-gesture-handler";
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class LoginRetro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      name_err: "",
      address_err: "",
      department_err: "",
      email: "",
      email_err: "",
      department: 0,
      pointer: 0,
      ip: "",
      images: [],
      user_avatar: "",
      departments_data: [],
      isLoading: false,
      loader: true,
      generals:""
    };
  }
  componentDidMount() {
    // fetch("https://swmc-be.herokuapp.com/api/admin/allDepartments", {
    //   method: "GET",
    //   headers: {
    //     "Content-Type": "application/json"
    //   }
    // })
    //   .then(res => res.json())
    //   .then(async response => {
    //     console.log("Departments =>");
    //     console.log(response.data);
    //     this.setState({ departments_data: response.data, loader: false })
    //   })
    //   .catch(error => alert("Please Check Your Internet Connection"));
  
     
  }

 
  // onDepartmentSelect(value) {
  //   console.log(value);

  //   console.log("check_selected_array_object");
  //   console.log(check_selected_array_object);
  //   this.setState({
  //     department: value
  //   });
  //   this.state.department = value;
  //   console.log("this.state.department")
  //   console.log(this.state.department)
  // }
  nextImage = () => {
    const { pointer, images } = this.state;
    let lengthOfArr = this.state.images.length;

    if (pointer == lengthOfArr - 1) {
      this.setState({ pointer: 0 });
    } else {
      this.setState({ pointer: pointer + 1 })
    }
    // alert(pointer);
  }
  backImage = () => {
    const { pointer, images } = this.state;
    let lengthOfArr = this.state.images.length;

    if (pointer == 0) {
      this.setState({ pointer: lengthOfArr - 1 });
    } else {
      this.setState({ pointer: pointer - 1 })
    }
    // alert(pointer);
  }
  submit = () => {

    const { user_avatar, name, email,
      //  department,
        ip } = this.state;

    let email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let l_email = email.toLowerCase();

    if (name == "" && email == "") {
      Alert.alert(
        "SWMC_app",
        "please enter all fields",
        [

          { text: "OK" }
        ],
        { cancelable: false }
      );
    }


    if (name == "") {
      this.setState({ name_err: "Required" });
    } else {
      this.setState({
        name_err: ""
      });
    }

    if (email == "") {
      this.setState({ email_err: "Required" });
    } else {
      if (l_email.indexOf("@swmc.com") == -1) {
        this.setState({
          email_err: "Invalid Email Address"
        });
      } else {
        this.setState({
          email_err: ""
        });
      }

    }
    // if (department == "") {
    //   this.setState({ department_err: "Required" });
    // } else {
    //   this.setState({
    //     department_err: ""
    //   });
    // }

    if (name != "" && email != "" && l_email.indexOf("@swmc.com") != -1
    //  && generals != ""
     ) {
      this.setState({ isLoading: true });
      // console.log(l_email+"--"+name+"--"+ip+"--"+department+"--"+this.state.images[this.state.pointer]);
      fetch(SWMC_USER + "user", {
        method: "POST",
        body: JSON.stringify({
          "email": l_email,
          "name": name,
          "image" : this.state.user_avatar,
          "ip" : ip,
          "department": "General",
          "phoneNumber" : "877878787",
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(async response => {
          // console.log("Response =>=?");
          // console.log(response);
          if (response.status != false) {
            // console.log("this.state.address")
            // console.log(this.state.address)
            // console.log(this.state.department)
            // // console.log("this.state.address")
            let arr = {};
            arr.address = this.state.address;
            arr.email = this.state.email;
            // arr.department = department;
            // var check_selected_array_object = this.state.departments_data.find(
            //   task => task._id === this.state.department
            // );
            // if(check_selected_array_object){

            //   arr.department= check_selected_array_object.name;
            // }else{
            //   arr.department= 'N/A';

            // }
            AsyncStorage.setItem("userID", JSON.stringify(response.data));
            AsyncStorage.setItem("user_image", JSON.stringify(this.state.user_avatar));
            AsyncStorage.setItem("other_data", JSON.stringify(arr));
            // // console.log("arr")
            // // console.log(arr)
            // AsyncStorage.setItem("user_image", JSON.stringify(this.state.images[this.state.pointer]));
            this.setState({ isLoading: false });
            this.props.navigation.push("ChatRetro")
          } else {
            Alert.alert("Sorry", response.msg, [{ text: "OK" }], {
              cancelable: true
            });
            this.setState({ isLoading: false });
          }
        })
        .catch(error => {
          // // console.log("Please Check Your Internet Connection");
          this.setState({ isLoading: false });
        });
    }
  }
  getPermissionAsync = async () => {
    if (Platform.OS !== 'web') {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    // // console.log(result);

    if (!result.cancelled) {
      this.setState({ user_avatar: result.uri })
      AsyncStorage.setItem("user_image", JSON.stringify(result.uri));
      // setImage(result.uri);
    }
  };

  render() {
    // const { navigation } = this.props.navigation;
    // const fontColor = "#82a601";
    // console.log("---------------")
    // console.log("http://swmcapp.com/"+this.state.images[this.state.pointer])
    // let image_path = "";
    // let image_uri = "";
    // image_uri = this.state.images[this.state.pointer]
    // if (image_uri) {
    //   let ispresant = image_uri.indexOf("http");
    //   // // console.log("-------1321313213--------")
    //   // // console.log(ispresant)
    //   if (ispresant == -1) {
    //     image_path = "http://swmcapp.com/" + this.state.images[this.state.pointer];
    //   } else {
    //     image_path = image_uri
    //   }
    // }

    return (


      <LinearGradient style={{ width: screenWidth, height: screenHeight }} colors={['#000', '#000']}>
        <KeyboardAwareScrollView
          contentContainerStyle={{ width: screenWidth, height: screenHeight }}
        >
          {/* <ImageBackground source={require("../assets/images/bcgrnd.png")} imageStyle={{resizeMode:"cover", overlayColor:"grey"}} style={{flex:1 }}> */}


          <View style={{ flex: 1 }}>
            <BaseHeader1
              navigation={this.props.navigation}
              title={"Customer Care"}
              logo={true}
            />

            <View
              style={{
                width: "100%",
                alignItems: "center",
                alignContent: "center"
              }}
            >
              <View
                style={{
                  width: "90%",
                  marginVertical: 10,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >

                {/* <View style={{marginTop:25  }}>
                            <TouchableOpacity style={{borderRadius:20,borderWidth:1,borderColor:"grey",padding:5,backgroundColor:"#fff",marginTop:10}} onPress={() => this.pickImage()}>
                                <Text>Choose Image</Text>
                            </TouchableOpacity>
                            {this.state.user_avatar? (<Image source={{ uri: this.state.user_avatar }} style={{width:80, height: 80,borderRadius:50,marginTop:10 }} />):null }
                            </View> */}


              </View>
            </View>
            <View style={{ paddingHorizontal: 25, marginTop: 15 }}>

            {/* <View style={styles.popbox}> */}
              <View style={{ width: "95%", justifyContent: "center", alignContent: "center", marginTop: 25, borderWidth: 2, borderColor: "#71b1f0", paddingTop: 5, paddingBottom: 5, borderRadius: 5, backgroundColor: 'rgba(238,238,238,0.2)' }}>
                <TextInput
                  style={{ fontSize: 12, paddingHorizontal: 10, fontWeight: "bold", color: "#fff",fontFamily:'space-mono' }}
                  placeholder="Full Name"
                  placeholderTextColor="#71b1f0"
                  onChangeText={(name) => {
                    this.setState({ name });
                  }}

                  value={this.state.name}
                />

              </View>
              {/* </View> */}



              <View>
                <Text style={{ color: "red" }}>{this.state.name_err}</Text>
              </View>
              <View style={{ width: "95%", justifyContent: "center", alignContent: "center", marginTop: 15, borderWidth: 2, borderColor: "#71b1f0", paddingTop: 5, paddingBottom: 5, borderRadius: 5, backgroundColor: 'rgba(238,238,238,0.2)' }}>
                <TextInput
                  style={{ fontSize: 12, paddingHorizontal: 10, fontWeight: "bold", color: "#fff",fontFamily:'space-mono' }}
                  placeholder="Email Address"
                  placeholderTextColor="#71b1f0"
                  onChangeText={(email) => {
                    this.setState({ email });
                  }}
                  value={this.state.email}
                />

              </View>
              <View>
                <Text style={{ color: "red" }}>{this.state.email_err}</Text>
              </View>
              {/* <View style={{ width: "95%", marginTop: 20 }}>
                <View
                  style={{
                    backgroundColor: 'rgba(238,238,238,0.2)',
                    borderWidth: 0.8,

                    borderColor: "#808080",
                    padding: 0,
                    marginTop: 10,

                  }}
                >
                  <Picker
                    mode="dropdown"

                    style={{ width: undefined, height: 40, color: "#fff" }}
                    placeholder="how we can help you ?"
                    placeholderStyle={{}}
                    placeholderIconColor="#fff"
                    textStyle={{ fontSize: 12 }}
                    selectedValue={this.state.department}
                    onValueChange={this.onDepartmentSelect.bind(this)}
                  >
                    <Picker.Item label="What can we help you with?" value={0} />
                    {this.state.departments_data.length > 0 ?
                      this.state.departments_data.map((item, index) => {
                        return (
                          <Picker.Item key={index} label={item.name} value={item.name} />
                        )
                      }) : null
                    }


                  </Picker>
                </View>
                <Text style={{ color: "#00ACD9" }}>{this.state.department_err}</Text>

              </View> */}

               {/* <View style={{ width: "100%",marginTop:100,alignItems:"center",alignContent:"center"}}>
               <TouchableOpacity  style={styles.login_button} onPress={() => this.submit()} >
               <Text style={{ color: '#eb7b65',fontSize:20 }}>Login</Text>
                </TouchableOpacity>
             </View>  */}






            </View>
           



    {/* <View style={{ width: "100%",marginTop:20,alignItems:"center"}}>
    <View style={{width:"90%",paddingLeft:5}}>
    <TouchableOpacity  style={styles.login_button} onPress={() => this.submit()} >
               <Text style={{ color: '#fff',fontSize:15,fontWeight:"bold" }}>Login</Text>
                </TouchableOpacity>
    </View>
           
             </View>  */}


          </View>
          <View style={styles.getstarted_button} >
            <TouchableOpacity style={{ width: "50%", alignItems: "center", }} onPress={() => this.submit()}>

              <View>
                <Text style={{  color: '#fff',fontSize:15,fontWeight:"bold",fontFamily:'space-mono' }}>
                  Login
    </Text>
              </View>


            </TouchableOpacity>
          </View>




          {/* </ImageBackground> */}
        </KeyboardAwareScrollView>

      </LinearGradient>



















    );
  }
}
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  search: {

    width: "100%",
    marginTop: 10,
    paddingVertical: 15,

    backgroundColor: "#F5F5F5",
    borderRadius: 50,
  },
  box: {

    width: "100%",
    height: 60,
    resizeMode: "cover",
    borderRadius: 80



  },
  login_button: {
    width: "93%",
    paddingVertical: 5,
    alignItems: "center",
    alignContent: "center",

    backgroundColor: "#74b5f5",
    borderRadius: 5,
  },
  getstarted_button: {
    width: "90%",
    // top: 80,
    marginHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: "#74b5f5",
    borderRadius: 10,

    bottom: 10


  },
  popbox: {
 
    shadowColor: "#71b1f0",
    shadowOffset: {
      width: 3,
      height: 12,
    },
    shadowOpacity: 10.4,
    shadowRadius: 6.62,
    elevation: 6,
  }
})





















































{/* {this.state.isLoading ? (
                 <Button
                   bordered
                   style={{
                     width: "100%",
                     alignItems: "center",
                     justifyContent: "center",
                     borderColor: fontColor,
                     borderRadius: 5,
                     alignItems:"center"
                   }}
                 >
                   <ActivityIndicator color={"#fd7e14"} size={"small"} />
                 </Button>
               ) : (
               
               <TouchableOpacity onPress={()=>this.props.navigation.navigate('ChatDashboard')}>
                   <image source={require('../assets/images/enter.png')}/>
                   </TouchableOpacity>
               )} */}
